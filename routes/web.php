<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\IframeController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Models\Quote;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $settings = [];
    return view('index',compact("settings"));
});

Route::get("/viewer",[IframeController::class, "view"])->name("viewer");

Route::get('/', [AuthController::class,'login']);

Route::get('/profile', [ProfileController::class,'profile']);

Route::post('/profile', [ProfileController::class,'profilePost']);

Route::get('/new-user', [ProfileController::class,'newUser']);

Route::get('/staff', [ProfileController::class,'staff'])->name("staff.index");

//Route::get('/staff/{project_id}', [ProfileController::class,'staff'])->name("assign-project");

Route::post('/user-post', [ProfileController::class,'userPost']);

Route::post('/user-change-password', [ProfileController::class,'userChangePasswordPost']);

Route::get('/login', [AuthController::class,'login'])->name('login');
Route::get('/signup', [AuthController::class,'signup']);
Route::get('/forgot-password', [AuthController::class,'forgotPassword']);
Route::get('/password-reset', [AuthController::class,'passwordReset']);

Route::get('/logout', [AuthController::class,'logout']);

Route::post('/login', [AuthController::class,'loginPost']);

Route::get('/dashboard', [DashboardController::class,'dashboard']);

Route::get('/add-project', [ProjectController::class,'addProject'])->name("add-project");

Route::post('/store-project', [ProjectController::class,'storeProject'])->name("store-project");

Route::get('/project-detail/{id}', [ProjectController::class,'projectDetails'])->name("project-detail");

Route::get('/project/{id}', [DashboardController::class,'editProject'])->name("edit-project");

Route::post('/update-project/{id}', [DashboardController::class,'updateProject'])->name("update-project");

//Route::post('/upload-image', [ImageController::class, 'dropZone' ])->name('drag-drop');

Route::resource('/modules', ModuleController::class);

Route::get('/modules/documentation/{module_id}', [ModuleController::class, "moduleDocumentation"])->name("module-documentation");

Route::resource("/jobs", JobController::class);

Route::get("/jobs-dashboard", [JobController::class,"dashboard"])->name("jobs-dashboard");

//Route::post("/take-gig/{module_id}", [JobController::class,"takeGig"])->name("take-gig");
Route::get("/take-gig/{module_id}", [JobController::class,"takeGig"])->name("take-gig");

Route::resource('/tasks', TaskController::class);

Route::get('/document-task/{id}', [TaskController::class,"document"])->name("document-task");

Route::post('/document-task/{id}', [TaskController::class,"documentTask"])->name("task.document");

Route::post('/modules/assign-module', [ModuleController::class, 'assignModule'])->name('assign-module');

Route::get('uploads', [FileUploadController::class, 'fileUploads' ])->name('fileUploads');

//Route::get("/")

//Route::post('file-upload', [FileUploadController::class, 'dropzoneFileUpload' ])->name('dropzoneFileUpload');
Route::post('file-upload', [FileUploadController::class, 'uploadZips' ])->name('dropzoneFileUpload');

Route::get('/test', function (){
    return \App\Models\Task::with(['module' => function($module){
        return $module->with("user");
    }])->get();
});

Route::get('/html-assets/{directory}/{page}', function (\Illuminate\Http\Request $request) {
    if (!$request->hasValidSignature()) {
        abort(401);
    }
});

Route::get("/store-quotes", function(){
     $response = Http::get("https://type.fit/api/quotes");
//     return $response->collect();
     foreach ($response->collect() as $res){
         Quote::create([
             "text" => $res['text'],
             "author" => $res['author'],
         ]);
     }
});


