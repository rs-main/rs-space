@extends('layouts.primary')
@section('content')

@section('head')
    <style>
        .chip{
            width: auto;
            color: black;
            background-color: white;
            border-radius: 7px;
            margin-right: 5px;
            margin-top: 5px;
        }
    </style>
@endsection


<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h6 class="mb-2">
                        {{__('ALL PROJECTS')}}
                    </h6>
                    <span>
                    </span>

                    <div class="page-header mb-4 border-radius-xl">
                        <span class="mask bg-gradient-dark"></span>
                        <div class="container">
                            <div class="row">

                                <div class="col-lg-6 my-auto">
                                    <h5 class="text-white fadeIn2 fadeInBottom mt-4">
                                        {{__($quote->text)}}
                                    </h5>
                                    <p class="text-white opacity-8 fadeIn3 fadeInBottom">{{__($quote->author)}} </p>
                                </div>
                            </div>

                            @if(Auth::user()->admin)
                                     <a  href="{{route('add-project')}}" type="button" class="btn bg-gradient-dark mt-4">{{__('Add New Project')}}</a>
                            @endif

                        </div>
                    </div>

                </div>

                <div class="card-body ">

                    <div class="row">

                        @if(sizeof($projects) < 1)
                            <h4 class="h4 text-center">NO PROJECT CREATED OR ASSIGNED YET!</h4>
                        @endif

                        @foreach($projects as $project)
                            <div class="col-lg-4 col-md-6 col-12 mt-4 mt-lg-0 mb-4">
                            <div class="card text-center">
                                <div class="overflow-hidden bg-gradient-secondary  position-relative border-radius-lg bg-cover p-3">
                                    <span class=" bg-gradient-light opacity-6"></span>
                                    <div class="card-body position-relative z-index-1 d-flex flex-column mt-5">
                                        <h4 class="text-white">{{__('Project')}}</h4>
                                        <h4 class="text-white">{{__($project->name)}}</h4>
                                        <a class="text-white text-sm font-weight-bold mb-0 icon-move-right mt-4" href="#">
                                            {{__('Read More')}}
                                            <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                                        </a>
                                        <div class="mt-4">
                                            <a href="{{route('project-detail',$project->id)}}" type="button" class="btn btn-outline-light">
                                                {{__('Open')}}

                                            </a>
                                            <a href="#" type="button" class="btn btn-light" onclick="handleCopyTextFromParagraph('{{$project->repository_url}}')">{{__('Clone')}}</a>

                                            @if(Auth::user()->admin)
                                                <a href="{{route('edit-project', $project->id)}}" type="button" class="btn btn-light" >{{__('Edit')}}</a>
                                            @endif

                                            <a href="{{route('modules.index',['project_id' => $project->id])}}" type="button" class="btn btn-light" >{{__('Modules')}}</a>

                                            @if(Auth::user()->admin)
                                            <a href="{{route('staff.index',['project_id' => $project->id,"action" => "assign"])}}" type="button" class="btn btn-light" >{{__('Assign To Resource')}}</a>
                                            @endif

                                            <h5 class="copied" style="display: none">
                                                {{$project->repository_url}}
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection

@section("script")

<script>
    function handleCopyTextFromParagraph(text) {
        const cb = navigator.clipboard;
        const paragraph = document.querySelector('h5');
        cb.writeText(text);
    }
</script>
@endsection
