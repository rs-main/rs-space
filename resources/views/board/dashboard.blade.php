@extends('layouts.primary')

@section('content')

    <div class="container-fluid py-4">
        <div class="card mt-4">
            <div class="card-header pb-0 p-3">
                <div class="d-flex align-items-center">
                    <h6 class="mb-0">{{__('Jobs Dashboard')}}</h6>

                </div>
            </div>

            <div class="table-responsive">
                <table class="table align-items-center mb-0">
                    <thead>
                    <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            {{__('Gig Name')}}
                        </th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                            {{__('Start Date')}}

                        </th>

                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">{{__('End Date')}}</th>

                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">{{__('Showed Interest')}}</th>

                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">{{__('Applied')}}</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($modules as $module)
                        <tr>
                            <td>
                                <div class="d-flex px-2 py-1">
                                    <div class="avatar avatar-sm me-3 bg-gradient-dark border-radius-md p-2">
                                        <h6 class="text-white">{{$module->name['0']}}</h6>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0 text-xs">{{$module->name}}</h6>

                                    </div>
                                </div>
                            </td>

                            <td class="align-middle text-center text-sm">
                                <span class="badge bg-gradient-success font-weight-bold">{{$module->start_date}}</span>
                            </td>
                            <td class="align-middle text-center text-sm">
                                <span class="badge bg-gradient-primary font-weight-bold">{{$module->end_date}}</span>
                            </td>

                            <td></td>
                            <td></td>

                        </tr>



                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection


@section('script')

    <script>
        "use strict"
        $(function () {
            $('.todo_checkbox').on('change',function () {
                let that = $(this);
                if(this.checked)
                {
                    $.post('/todos/change-status',{
                        id: that.attr('data-id'),
                        status: 'Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
                else{
                    $.post('/todos/change-status',{
                        id: that.attr('data-id'),
                        status: 'Not Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
            });
        });
    </script>
    <script>

        $(function () {
            $('.goal_checkbox').on('change',function () {
                let that = $(this);
                if(this.checked)
                {
                    $.post('/goals/change-status',{
                        id: that.attr('data-id'),
                        status: 'Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
                else{
                    $.post('/goals/change-status',{
                        id: that.attr('data-id'),
                        status: 'Not Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
            });
        });
    </script>


@endsection



