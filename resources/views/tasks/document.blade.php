@php

@endphp

@extends('layouts.primary')


@section('content')

    <div class="card">
        <div class="card-header">
            {{__('Add Documentation to Task')}}
        </div>

        <div class="card-body">
            <form action="{{route('task.document',$task_id)}}" method="post">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="mb-3">
                        <label for="description">{{__('Document Task')}}</label><span class="text-danger">*</span>
                        <textarea class="form-control" name="documentation" id="description" rows="20">
                            {{ $documentation }}
                        </textarea>
                </div>

                    <input type="hidden" name="id" value="{{$task_id}}">

                @csrf

                    <button type="submit" class="btn bg-gradient-faded-success">
                        {{__('Save')}}
                    </button>

                <a href="{{route("tasks.index")}}" type="button" class="btn bg-gradient-primary">
                    {{__('Close')}}
                </a>
            </form>

        </div>

    </div>

@endsection
@section('script')

    <script>
        "use strict";
        $(function () {


            flatpickr("#date", {

                dateFormat: "Y-m-d",
            });

        });


    </script>
    <script>
        tinymce.init({
            selector: '#description',
            plugins: 'table,code,image',


        });
    </script>


@endsection








