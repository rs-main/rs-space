@extends('layouts.primary')
@section('content')

@section("head")
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@endsection

    <div class="page-header mb-4 border-radius-xl">
        <span class="mask bg-gradient-dark"></span>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 my-auto">

                    <h5 class="text-white fadeIn2 fadeInBottom mt-4">{{__('“A dream without a plan is just a wish.”')}}
                    </h5>

                </div>
            </div>

            <a href="{{$create_url}}" type="button" class="btn bg-gradient-light">{{__('Add New Task')}}</a>

        </div>
    </div>

    <div class="row">

        <div class="col-md-12 mt-4">
            <div class="card">

                <div class="card mt-4">
                    <div class="card-header pb-0 p-3">
                        <div class="d-flex align-items-center">
                            <h6 class="mb-0">{{__('Recent Tasks')}}</h6>

                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center mb-0">
                            <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    {{__('Task Name')}}
                                </th>

                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    {{__('Task Summary')}}
                                </th>

                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">
                                    {{__('Task Points')}}
                                </th>

                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">{{__('Dates')}}</th>


                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">{{__('Action')}}</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($tasks as $project)
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1 text-center">
                                            <div class="avatar avatar-sm me-3 bg-gradient-dark border-radius-md p-2">
                                                <h6 class="text-white">{{$project->name['0']}}</h6>
                                            </div>
                                            <div class="d-flex flex-column justify-content-center text-center">
                                                <h6 class="mb-0 text-xs">{{$project->name}}</h6>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="d-flex px-2 py-1 text-center">

                                            <div class="d-flex flex-column justify-content-center text-center">
                                                <h6 class="mb-0 text-xs">{{$project->summary}}</h6>
                                            </div>
                                        </div>
                                    </td>

                                    <td class="align-left text-center text-sm">
                                        <span class="badge bg-gradient-success font-weight-bold ">{{$project->points}}</span>
                                    </td>

{{--                                    <td class="align-middle text-center text-sm">--}}
{{--                                        <span class="badge bg-gradient-success font-weight-bold ">{{$project->start_date}}</span>--}}
{{--                                    </td>--}}

                                    <td class="align-middle text-center text-sm">
                                        <span class="badge bg-gradient-success font-weight-bold ">{{$project->start_date}}</span>
                                        <br>
                                        <span class="badge bg-gradient-primary font-weight-bold">{{$project->end_date}}</span>
                                    </td>


                                    <td class="align-middle text-center text-sm">

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-dark">{{$project->status}}</button>
                                            <button type="button" class="btn btn-outline-dark dropdown-toggle dropdown-toggle-split"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu bg-outline-dark" >
                                                @php $task_id = $project->id; @endphp
                                                <a class="dropdown-item" onclick="changeStatus('Pending',{{$task_id}})">Pending</a>
                                                <a class="dropdown-item"  onclick="changeStatus('Started',{{$task_id}})">Started</a>
                                                <a class="dropdown-item"  onclick="changeStatus('Finished', {{$task_id}})">Finished</a>
                                            </div>
                                        </div>

                                        <br>
                                        <a href="{{route("document-task",$task_id)}}" class="btn btn-dark">DOCUMENT TASK</a>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

    </div>

@endsection

@section("script")

    <script>
         function changeStatus(status,id){
            fetch(`/tasks/${id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({status: status})
            }).then(function(){
                location.reload();
            });

            console.log(content);
        }
    </script>


@endsection
