@extends('layouts.primary')


@section('content')

    <div class="card">
        <div class="card-header">
            {{__('Add Task to Module')}}
        </div>

        <div class="card-body">
            <form action="{{route('tasks.store')}}" method="post">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="example-text-input" class="form-control-label">
                        {{__('Name')}}

                    </label><span class="text-danger">*</span>
                    <input class="form-control" name="name" type="text" id="example-text-input">
                </div>

                <div class="form-group">
                        <label for="example-text-input" class="form-control-label">
                            {{__('Summary')}}

                        </label><span class="text-danger">*</span>
                        <input class="form-control" name="summary" type="text" id="example-text-input">
                </div>

                <div class="form-group">
                        <label for="module_id" class="form-control-label">
                            {{__('Module')}}

                        </label><span class="text-danger">*</span>

                        <select class="form-select" name="module_id">
                            <option value="">Choose Module</option>
                           @foreach ($modules as $module)
                                <option @if ($module_id == $module->id) selected @endif value="{{$module->id}}">{{$module->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-6">
                                <label for="points" class="form-control-label">
                                    {{__('Allocate Points')}}

                                </label><span class="text-danger">*</span>
                                <input type="number" name="points" placeholder="Points for this task" id="points" class="form-control" />
                            </div>

                        </div>

                    </div>


                <div class="mb-3">
                        <label for="description">{{__('Description')}}</label><span class="text-danger">*</span>
                        <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="start_date" class="form-control-label">{{__('Start Date')}}</label>
                                <input class="form-control" type="date" name="start_date" id="date">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="end_date" class="form-control-label">{{__('End Date')}}</label>
                                <input class="form-control" type="date" name="end_date" id="date">
                            </div>
                        </div>
                    </div>

                @csrf

                    <button type="submit" class="btn bg-gradient-faded-success">
                        {{__('Save and Continue')}}
                    </button>

                <button type="submit" name="exit"  value="exit" class="btn bg-gradient-secondary">
                    {{__('Save and Exit')}}
                </button>

{{--                <button type="button" class="btn bg-gradient-primary">--}}
{{--                    {{__('Close')}}--}}
{{--                </button>--}}
            </form>

        </div>

    </div>

@endsection
@section('script')

    <script>
        "use strict";
        $(function () {


            flatpickr("#date", {

                dateFormat: "Y-m-d",
            });

        });


    </script>
    <script>
        tinymce.init({
            selector: '#description',

            plugins: 'table,code',


        });
    </script>


@endsection








