@extends('layouts.primary')

@section('head')
{{--    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
{{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" rel="stylesheet">--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js"></script>--}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.6.0/bootstrap-tagsinput.min.css" integrity="sha512-X6069m1NoT+wlVHgkxeWv/W7YzlrJeUhobSzk4J09CWxlplhUzJbiJVvS9mX1GGVYf5LA3N9yQW5Tgnu9P4C7Q==" crossorigin="anonymous" referrerpolicy="no-referrer" />


@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    {{ __('  Add New Project') }}

                    <a href="/dashboard" type="submit" class="btn bg-gradient-secondary float-end">{{ __('Go to Projects') }}</a>
                </div>

                <div class="card-body">
                    <form action="{{route('store-project')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger" style="color: white">
                                <ul class="list-unstyled">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Name') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="name" type="text" id="title" value="{{old('name')}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1">{{ __('Description') }}</label><span class="text-danger">*</span>
                            <textarea class="form-control" name="description" id="description" rows="3">  </textarea>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Repository Url') }}</label><span class="text-danger">*</span>
                            <select class="form-control" name="repository_url">
                                <option>Select Project</option>
                                @foreach($repositories as $repository)
                                    <option value="{{$repository["links"]["clone"][0]["href"]}}">{{$repository["full_name"]}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Developer Guide') }}</label><span class="text-danger">*</span>
                            <br>
                            <span class="small"></span>
                            <select class="form-control" name="developer_guide_folder_id" id="folder_id">
                                <option>Select Folder</option>
                                @foreach($htmlFiles as $htmlFile)
                                    <option value="{{$htmlFile->id}}">{{$htmlFile->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="col-6">
                            <label>{{__('Specific pages')}}</label>
                                <select class="select-pages" name="pages[]" multiple="multiple" style="width: 200%; height: 120px"></select>
                            </select>
                        </div>

                        <br>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Asset Library Documentation') }}</label><span class="text-danger">*</span>

                            <select class="form-control" name="asset_library_doc_folder_id" id="asset_library_doc_folder_id">
                                <option>Select Folder</option>
                                @foreach($htmlFiles as $htmlFile)
                                    <option value="{{$htmlFile->id}}">{{$htmlFile->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('User Experience Guide (pdf)') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="user_guide" type="file" id="user_guide" value="{{old('user_guide')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Wireframe (Draw.io) image') }}</label><span class="text-danger">  </span>
                            <input class="form-control"  name="wireframe_url" type="file" id="wireframe_url" value="{{old('wireframe_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Design Asset link (Adobe Xd/Protopie)') }}</label><span class="text-danger"></span>
                            <input class="form-control"  name="adobe_protopie_link" type="text" id="adobe_protopie_link" value="{{old('adobe_protopie_link')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project API EndPoints Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="api_endpoints_url" type="text" id="doc_base_url" value="{{old('api_endpoints_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Dev Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="dev_url" type="text" id="dev_url" value="{{old('dev_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-date-input" class="form-control-label">{{ __('Date') }}</label>
                            <input class="form-control" name="date" type="date" value="{{date('Y-m-d')}}" id="date">
                        </div>

                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn bg-gradient-secondary" id="submitBtn">{{ __('Save') }}</button>
                        <button type="button" class="btn bg-gradient-primary">{{ __('Close') }}</button>
                    </form>

                </div>

            </div>
        </div>


            <div class="col-md-6">
                <div class="card">
                    <div class="card-header p-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h6 class="mb-0">{{ __('Project list') }}</h6>
                            </div>

                        </div>
                        <hr class="horizontal dark mb-0">
                    </div>
                    <div class="card-body p-3 pt-0">
                        <ul class="list-group list-group-flush" data-toggle="checklist">
                            @foreach($projects as $project)
                                <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                                    <div class="checklist-item checklist-item-primary ps-2 ms-3">
                                        <div class="d-flex align-items-center">


                                            <div class="form-check">
                                                <input class="form-check-input todo_checkbox" type="checkbox"
                                                       data-id=""
                                                >

                                            </div>
                                            <h6 class="mb-0 text-dark font-weight-bold text-sm">{{$project->name}}</h6>
                                            <div class="dropdown float-lg-end ms-auto pe-4">
                                                <a href="javascript:;" class="cursor-pointer" id="dropdownTable2" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>Delete</a>
                                                    <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                                </a>

                                            </div>
                                        </div>
                                        <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                                            <div>
                                                <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                                                <span class="text-xs font-weight-bolder"></span>
                                            </div>

                                        </div>
                                    </div>
                                    <hr class="horizontal dark mt-4 mb-0">
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

            </div>
    </div>

@endsection


@section('script')

    <script>
        "use strict";
        $(function () {


            flatpickr("#date", {

                dateFormat: "Y-m-d",
            });

        });




    </script>

    <script>
        tinymce.init({
            selector: '#description',


            plugins: 'table,code',


        });
    </script>
    <script>
        $(function () {
            $('.todo_checkbox').on('change',function () {
                let that = $(this);
                if(this.checked)
                {
                   $.post('/todos/change-status',{
                       id: that.attr('data-id'),
                       status: 'Completed',
                       _token: '{{csrf_token()}}',
                   });
                }
                else{
                    $.post('/todos/change-status',{
                        id: that.attr('data-id'),
                        status: 'Not Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
            });
        });
    </script>

    <script>

        $(".select-pages").select2();

        let folder_id = "";

        $("#folder_id").on("change",function (){
            folder_id = $(this).val();
            console.log(folder_id);
            $(".select-pages").select2({
                placeholder: 'Type page name...',
                minimumInputLength: 1,
                theme: "classic",
                allowClear: true,
                ajax: {
                    url: `/api/files/${folder_id}`,
                    dataType: 'json',
                    data: function(params) {
                        return {
                            q: params.term
                        }
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        })


    </script>

    <script>
        const daySelect = document.getElementById('select-pages');
        daySelect.options[daySelect.options.length] = new Option('Text 1', 'Value1');
        fetch('/api/files/html-demo')
            .then(response => response.json())
            .then(data => {
                $('#select-pages').html(data.data);
            });
    </script>

    <script>
        const submitBtn = $("#submitBtn");

        function disableButton(){
            submitBtn.html("Uploading...");
            setTimeout(function (){
                submitBtn.prop("disabled",true);
            },500)
        }
    </script>




@endsection




