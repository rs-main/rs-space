@extends('layouts.primary')
@section('content')

    <style>
        .chip{
            width: auto;
            color: white;
            background-color: #2F3459;
            border-radius: 7px;
            margin-right: 5px;
            margin-top: 5px;
            padding: 6.5px;
        }
    </style>


<div class="container-fluid my-3 py-3" style="height: 100%">
    <div class="row mb-5" >
        <div class="col-lg-2">
            <div class="card position-sticky top-1">
                <ul class="nav flex-column bg-white border-radius-lg p-3">

                   @include("projects.partials.detail-menu")

                </ul>
            </div>
        </div>

        <div class="col-lg-10 mt-lg-0" >

            <div class="card mb-5" id="brief-description" style="height: 650px; overflow-y: scroll;">
<!--                <div class="card-header">-->
<!--                    {{__('Brief Description of Project')}}-->
<!--                </div>-->


                <div class=" card-body" >
                    <div class="page-header mb-4 border-radius-xl">
                        <span class="mask bg-gradient-dark"></span>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 mt-6 mb-6 text-center my-auto">
                                    <h3 class="text-white">
                                        Real Studios
                                    </h3>
                                    <h2 class="text-white fadeIn2 fadeInBottom mt-4">Project Documentation</h2>
                                    <h5 class="text-white fadeIn2 fadeInBottom">{{$project->created_at}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 text-center mt-8 mb-7">
                        <h5 class=" fadeIn2 fadeInBottom">Prepared By</h5>
                        <h3>Admin</h3>
                        <h6 class="text-muted"><a href="#"></a></h6>
                    </div>

                  {!! $project->description !!}

            </div>
            </div>

            <div class="card mt-20" id="developer" style="padding-bottom: 80px;">
                <div class="card-header">
                    <h2>{{__('Developer Guide')}}</h2>
                </div>

                <div class="card-body" style="height: 850px">

                    <iframe src="{{"/html-assets/".$project->developerGuideFolder->files[0]["file_url"]}}"
                            width="100%" height="100%">
                    </iframe>

                    <div style="margin-bottom: 10px;">

                    @foreach($specificPages as $file)
                            @php
                                //$viewer = \App\Helpers::getTemporaryUrl("viewer",["url" => url('/html-assets/'.substr($file['file_url'],0,-5))]);

                                $viewer = \App\Helpers::getTemporaryUrl("viewer",["url" => url('/html-assets/'.$file['file_url'])]);
                            @endphp

                            <a target="_blank" class="chip flex-column"
                           href="{!! urldecode($viewer) !!}">{{$file["file_name"]}}

                        </a>

                    @endforeach

                    </div>

                </div>
            </div>

                <div class="card mt-4 mb-4" id="asset-documentation">
                    <div class="card-header">
                        <h2>{{__('Asset Library Documentation')}}</h2>
                    </div>

                    <div class="card-body" style="height: 650px; padding-bottom: 50px;">

                        <iframe src="{{"/html-assets/".$project->assetGuideFolder->files[0]["file_url"]}}"
                                width="100%" height="100%">

                        </iframe>

                        <a style="padding-left: 30px; " target="_blank" href="/viewer?url={{$project->doc_base_url}}">View</a>

                    </div>

                </div>

            <div class="card mt-4 mb-4" id="user-guide">
                <div class="card-header">
                    <h2>{{__('User Guide')}}</h2>
                </div>

                <div class="card-body" style="height: 650px; padding-bottom: 50px;">
                    <iframe src="/pdf/{{$project->user_guide}}"
                        width="100%" height="100%">
                    </iframe>

                    @php
                        $url = url("pdf/".$project->user_guide);
                    @endphp

                    <a target="_blank" href="/viewer?url={{$url}}">View</a>
                </div>

            </div>

            <div class="card mt-4 mb-4" id="api-endpoints">
<!--                <div class="card" id="developer">-->
                    <div class="card-header">
                        <h2>{{__('API Endpoints')}}</h2>
                    </div>

                    <div class="card-body" style="height: 650px">
<!--                        <iframe width="100%" height="100%" allowfullscreen src="https://dextraclass.com/api/documentation"></iframe>-->
                        <iframe width="100%" height="100%" allowfullscreen src="{{$project->api_endpoints_url}}"></iframe>
                </div>

            </div>

            <div class="card" id="wire-frame">
                <div class="card-header">
                    <h2>{{__('WireFrame')}}</h2>
                </div>

                <div class="card-body" style="height: 650px; padding-bottom: 50px;">
                    <img src="{{"/images/".$project->wireframe_url}}" width="100%" height="100%" />
                </div>

                <a style="padding-left: 20px; padding-bottom: 20px;" target="_blank" href="/viewer?url={{url("/images/").'/'.$project->wireframe_url}}">View</a>


            </div>

            <div class="card mt-5" id="adobe-protopie" style="height: 650px;">
                <div class="card-header">
                    <h2>{{__('Adobe XD / PROTOPIE')}}</h2>
                </div>

                <iframe width="100%" height="100%" allowfullscreen src="{{$project->adobe_protopie_link}}"></iframe>

                <a style="padding-left: 20px; padding-bottom: 20px;" target="_blank" href="/viewer?url={{$project->adobe_protopie_link}}">View fullscreen</a>


            </div>

            <div class="card mt-5" id="comments">
                <div class="card-header pb-0 px-3">
                    <h2 class="mb-0">{{__('Documentation')}}</h2>
                </div>

                <div class="card-body pt-4 p-3">
                    <ul class="list-group">

                        @foreach($tasks as $task)

                        <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                            <div class="d-flex flex-column">
                                <h6 class="mb-3 text-sm">{{$task->name}}</h6>
                                <span class="text-xs mb-4">
{{--                                    {{__($task->userTask)}}--}}

                                    {!! $task->userTask? $task->userTask->documentation : "" !!}


                                    <span class="text-dark ms-sm-2 font-weight-bold">

                                    </span></span>

{{--                                <span class="text-xs">{{__('Date:')}} <span class="text-dark ms-sm-2 font-weight-bold"></span>--}}

                                </span>
                            </div>
                            <div class="ms-auto text-end">

{{--                                <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>{{__('Delete')}}</a>--}}
{{--                                <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>{{__('Edit')}}</a>--}}
                            </div>
                        </li>

                        @endforeach


{{--                        <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">--}}
{{--                            <div class="d-flex flex-column">--}}
{{--                                <h6 class="mb-3 text-sm"></h6>--}}
{{--                                <span class="text-xs mb-4">{{__('Comment three')}} <span class="text-dark ms-sm-2 font-weight-bold">--}}

{{--                                    </span></span>--}}

{{--                                <span class="text-xs">{{__('Date:')}} <span class="text-dark ms-sm-2 font-weight-bold">--}}

{{--                                    </span>--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                            <div class="ms-auto text-end">--}}

{{--                                <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>{{__('Delete')}}</a>--}}
{{--                                <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>{{__('Edit')}}</a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                        <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">--}}
{{--                            <div class="d-flex flex-column">--}}
{{--                                <h6 class="mb-3 text-sm"></h6>--}}
{{--                                <span class="text-xs mb-4">{{__('Comment three')}} <span class="text-dark ms-sm-2 font-weight-bold">--}}

{{--                                    </span></span>--}}

{{--                                <span class="text-xs">{{__('Date:')}} <span class="text-dark ms-sm-2 font-weight-bold"></span>--}}
{{--                                </span>--}}


{{--                            </div>--}}
{{--                            <div class="ms-auto text-end">--}}

{{--                                <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>{{__('Delete')}}</a>--}}
{{--                                <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>{{__('Edit')}}</a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </div>


        </div>
    </div>

</div>

@endsection

@section('script')

<script>
    "use strict";
    $(function () {
        flatpickr("#date", {
            dateFormat: "Y-m-d",
        });

    });


</script>
<script>
    tinymce.init({
        selector: '#description',

        plugins: 'table,code',


    });
</script>


@endsection

