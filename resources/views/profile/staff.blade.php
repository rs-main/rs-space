@extends('layouts.primary')
@section('content')


    <div class="card mb-3 mt-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-md-6 d-flex align-items-center">
                    <div class="col-md-6 text-left">
                       <h6>{{__('Staff')}}</h6>
                    </div>

                </div>
                <div class="col-md-6 text-right">
                    <a class="btn bg-gradient-dark mb-0" href="/new-user"><i class="fas fa-plus"></i>&nbsp;&nbsp;
                       {{__(' Add New User')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body  p-3">

        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4">

            </div>
        </div>
    </div>

    <div class="row">
        @foreach($staffs as $staff)
        <div class="col-md-4 mb-4">
            <div class="card card-pricing">
                <a href="{{url('edit-staff')}}">
                <div class="card-header bg-gradient-light text-center pt-4 pb-5 position-relative">
                    <div class="z-index-1 position-relative ">
                        <h5 class="mt-6"> Developer</h5>
                        <h3 class=" text-muted mt-2 mb-0 ">
                            {{strtoupper($staff->development_role)}}</h3>
{{--                        <h6 class="text-muted">2021-07-31</h6>--}}
                        <h6 class="text-muted">{{$staff->skill_level}}</h6>
                    </div>
                </div>
                <div class="text-center mt-4">
{{--                    <p class="text-muted">Prepared by</p>--}}
                    <h5>{{$staff->first_name}} {{$staff->last_name}}</h5>

                    <h6 class="text-muted"><a href="#">{{$staff->email}}</a></h6>
                </div>
                <div class="card-body text-center">

                    <a href="{{url('/modules?page=projects&staff='.$staff->id )}}" type="button" class="btn bg-gradient-dark mb-3">
                        Projects
                    </a>
                    <a class="btn bg-gradient-secondary mb-3" href="#">Send Message</a>

                    <a class="btn bg-gradient-faded-info mb-3" href="{{url('/modules?staff='.$staff->id)}}">
{{--                        <i class="fas fa-suitcase"></i>&nbsp;&nbsp;--}}
                        {{__(' Assign module')}}
                    </a>
{{--                    <a href="http://localhost/viewer?url={{url($staff->cv)}}" target="_blank" type="button" class="btn bg-gradient-warning">VIEW CV</a>--}}
                </div>
                </a>
            </div>
        </div>

        @endforeach
    </div>


@endsection
