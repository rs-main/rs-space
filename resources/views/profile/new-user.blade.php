@extends('layouts.primary')
@section('content')

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="multisteps-form mb-5">
                    <!--progress bar-->

                    <!--form panels-->
                    <div class="row">
                        <div class="col-12 col-lg-8 m-auto">
                            <form action="/user-post"  method="post" class="multisteps-form__form mb-8" enctype="multipart/form-data">
                                <!--single form panel-->

                                @if ($errors->any())
                                    <div class="alert alert-danger" style="color:white;">
                                        <ul class="list-unstyled">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active" data-animation="FadeIn">
                                    <h5 class="font-weight-bolder mb-0">
                                        {{__('Add New User')}}

                                    </h5>

                                    <div class="multisteps-form__content">
                                        <div class="row mt-3">
                                            <div class="col-12 col-sm-6">
                                                <label>{{__('First Name')}}</label>
                                                <input name="first_name"class="multisteps-form__input form-control" type="text"
                                                value="{{old('first_name')}}"  />
                                            </div>
                                            <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                                                <label>{{__('Last Name')}}</label>
                                                <input name="last_name"class="multisteps-form__input form-control" type="text"
                                                        value="{{old('last_name')}}"  />
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <label>{{__('Username/Email')}}</label>
                                                <input name="email" class="multisteps-form__input form-control" type="email"
                                                       value="{{old('email')}}"   />
                                            </div>

                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <label>{{__('Password')}}</label>

                                                <input name="password"  type="password" class="multisteps-form__input form-control"/>
                                                <p class="text-xs">
                                                    {{__('Keep blank if you do not want to change Password')}}
                                                    </p>

                                            </div>

                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                                                <label>{{__('Mobile Phone')}}</label>
                                                <input name="mobile_number" class="multisteps-form__input form-control"
                                                       value="{{old('mobile_number')}}" >
                                            </div>
                                            <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                                                <label>{{__('Telephone')}}</label>
                                                <input name="phone_number"class="multisteps-form__input form-control"
                                                       value="{{old('phone_number')}}"  />
                                            </div>


                                        </div>

                                        <div class="row mt-3">
                                            <div class="col-12 col-sm-12 mt-3 mt-sm-0">
                                                <label>{{__('Photo')}}</label>
                                                <input type="file" name="photo" class="multisteps-form__input form-control">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="card mt-4 multisteps-form__panel p-3 border-radius-xl bg-white" data-animation="FadeIn">
                                    <h5 class="font-weight-bolder">{{__('Skills')}}</h5>

                                    <div class="multisteps-form__content">
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <label>{{__('Skill Level')}}</label>
{{--                                                <input type="text" class="form-control" name="role" placeholder="Your role"/>--}}
                                                <select class="form-control" name="skill_level">
                                                    <option value="">Select Experience Level</option>
                                                    <option value="killer">Killer (Mid-Level)</option>
                                                    <option value="assassin">Assassin (Mid-Level-Upper)</option>
                                                    <option value="ninja">Ninja (Senior-Level)</option>
                                                    <option value="sensei">Sensei (Veteran Coder)</option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="multisteps-form__content">
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <label>{{__('Development Role')}}</label>
{{--                                                <input type="text" class="form-control" name="role" placeholder="Your role"/>--}}
                                                <select class="form-control" name="development_role">--}}
                                                        <option value="">Select Role</option>
                                                        <option value="android">Android</option>
                                                        <option value="backend">Backend</option>
                                                        <option value="frontend">Frontend</option>
                                                        <option value="ios">IOS</option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>

{{--                                    <div class="multisteps-form__content">--}}

{{--                                        <div class="row mt-3 skill-level" id="skill-level" >--}}
{{--                                                <div class="col-6">--}}
{{--                                                    <label>{{__('Core Competences')}}</label>--}}
{{--                                                    <select class="select-skills" name="skills[]" multiple="multiple" style="width: 100%; height: 120px"></select>--}}
{{--                                                </div>--}}

{{--                                                <div class="col-6">--}}
{{--                                                    <label>{{__('Level')}}</label>--}}
{{--                                                    <select class="form-control" name="skill_one" >--}}
{{--                                                        <option value="">Select Level</option>--}}
{{--                                                        <option value="killer">Killer (Mid-Level)</option>--}}
{{--                                                        <option value="assassin">Assassin (Mid-Level-Upper)</option>--}}
{{--                                                        <option value="ninja">Ninja (Senior-Level)</option>--}}
{{--                                                        <option value="sensei">Sensei (Veteran Coder)</option>--}}
{{--                                                    </select>--}}
{{--                                                </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="row mt-3" >--}}
{{--                                            <div class="col-6">--}}
{{--                                                <label>{{__('Core Competences')}}</label>--}}
{{--                                                <select class="select-skills" name="skills_two[]" multiple="multiple" style="width: 100%; height: 120px"></select>--}}
{{--                                            </div>--}}

{{--                                            <div class="col-6">--}}
{{--                                                <label>{{__('Level')}}</label>--}}
{{--                                                <select class="form-control" name="skill_two">--}}
{{--                                                    <option value="">Select Level</option>--}}
{{--                                                    <option value="killer">Killer (Mid-Level)</option>--}}
{{--                                                    <option value="assassin">Assassin (Mid-Level-Upper)</option>--}}
{{--                                                    <option value="ninja">Ninja (Senior-Level)</option>--}}
{{--                                                    <option value="sensei">Sensei (Veteran Coder)</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="row mt-3" >--}}

{{--                                            <div class="col-6">--}}
{{--                                                <label>{{__('Core Competences')}}</label>--}}
{{--                                                <select class="select-skills" name="skills[]" multiple="multiple" style="width: 100%; height: 120px"></select>--}}
{{--                                            </div>--}}

{{--                                            <div class="col-6">--}}
{{--                                                <label>{{__('Level')}}</label>--}}
{{--                                                <select class="form-control" name="skill_three">--}}
{{--                                                    <option value="">Select Level</option>--}}
{{--                                                    <option value="killer">Killer (Mid-Level)</option>--}}
{{--                                                    <option value="assassin">Assassin (Mid-Level-Upper)</option>--}}
{{--                                                    <option value="ninja">Ninja (Senior-Level)</option>--}}
{{--                                                    <option value="sensei">Sensei (Veteran Coder)</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                        <div class="row mt-3" >--}}

{{--                                            <div class="col-6">--}}
{{--                                                <label>{{__('Core Competences')}}</label>--}}
{{--                                                <select class="select-skills" name="skills[]" multiple="multiple" style="width: 100%; height: 120px"></select>--}}
{{--                                            </div>--}}

{{--                                            <div class="col-6">--}}
{{--                                                <label>{{__('Level')}}</label>--}}
{{--                                                <select class="form-control" name="skill_four">--}}
{{--                                                    <option value="">Select Level</option>--}}
{{--                                                    <option value="killer">Killer (Mid-Level)</option>--}}
{{--                                                    <option value="assassin">Assassin (Mid-Level-Upper)</option>--}}
{{--                                                    <option value="ninja">Ninja (Senior-Level)</option>--}}
{{--                                                    <option value="sensei">Sensei (Veteran Coder)</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                        <button type="button" class="btn btn-success mt-3 add-skill-btn">Add New Skill</button>--}}

{{--                                    </div>--}}
                                </div>

                                <div class="card mt-4 multisteps-form__panel p-3 border-radius-xl bg-white" data-animation="FadeIn">
                                    <h5 class="font-weight-bolder">{{__('CV')}}</h5>
                                    <div class="multisteps-form__content">
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <label>{{__('Attach CV')}}</label>
                                                <input type="file" class="form-control" name="cv"/>
                                            </div>

                                        </div>

                                    </div>
                                </div>


                                <div class="card mt-4 multisteps-form__panel p-3 border-radius-xl bg-white" data-animation="FadeIn">
                                    <h5 class="font-weight-bolder">{{__('Socials')}}</h5>
                                    <div class="multisteps-form__content">
                                        <div class="row mt-3">

{{--                                            <div class="col-12">--}}
{{--                                                <label>{{__('Twitter Handle')}}</label>--}}
{{--                                                <input  name="twitter" class="multisteps-form__input form-control" type="text"--}}
{{--                                                        value="{{old('twitter')}}" />--}}
{{--                                            </div>--}}
{{--                                            --}}
{{--                                            <div class="col-12 mt-3">--}}
{{--                                                <label>{{__('Facebook Account')}}</label>--}}
{{--                                                <input name="facebook" class="multisteps-form__input form-control" type="text"--}}
{{--                                                       value="{{old('facebook')}}" />--}}
{{--                                            </div>--}}
{{--                                            --}}
                                            <div class="col-12 mt-3">
                                                <label>{{__('Linkedin Account')}}</label>
                                                <input name="linkedin" class="multisteps-form__input form-control"
                                                 value="{{old('linkedin')}}"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!--single form panel-->
                                @csrf

                                @if (!empty($selected_user))
                                    <input type="hidden" name="id" value="{{$selected_user->id}}">
                                @endif
                                <div class="button-row text-left mt-4">
                                    <button class="btn bg-gradient-dark ms-auto mb-0 js-btn-next" type="submit" title="Next">{{__('Submit')}}</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('script')

    <script>
        $(".select-skills").select2({
            placeholder: 'Type skill name...',
            minimumInputLength: 3,
            theme: "classic",
            allowClear: true,
            ajax: {
                url: '/api/skills',
                dataType: 'json',
                data: function(params) {
                    return {
                        q: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        // $(".add-skill-btn").off("click").on("click",function(){
        //
        //     $(".skill-level").last().clone().appendTo("#skill-level");
        // })

        // function addrow()
        // {
        //     // var row = $("#table1 tr:last");
        //     var row =  $(".skill-level").last();
        //
        //     row.find(".select-skills").each(function(index)
        //     {
        //         $(this).select2('destroy');
        //     });
        //
        //     var newrow = row.clone();
        //
        //     $("#table1").append(newrow);
        //
        //     $("select.select2").select2();
        // }

    </script>

{{--    $myClone = $("section.origen").clone();--}}

{{--    $myClone.find("span").remove();--}}
{{--    $myClone.find("select").select2();--}}

@endsection

