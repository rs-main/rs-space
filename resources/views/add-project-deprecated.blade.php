@extends('layouts.primary')

@section('head')
{{--    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js"></script>

@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    {{ __('  Add New Project') }}

                    <a href="/dashboard" type="submit" class="btn bg-gradient-secondary float-end">{{ __('Go to Projects') }}</a>
                </div>

                <div class="card-body">
                    <form action="{{route('store-project')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger" style="color: white">
                                <ul class="list-unstyled">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Name') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="name" type="text" id="title" value="{{old('name')}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1">{{ __('Description') }}</label><span class="text-danger">*</span>
                            <textarea class="form-control" name="description" id="description" rows="3">  </textarea>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Repository Url') }}</label><span class="text-danger">*</span>
                            <select class="form-control" name="repository_url">
                                <option>Select Project</option>
                                @foreach($repositories as $repository)
                                    <option value="{{$repository["links"]["clone"][0]["href"]}}">{{$repository["full_name"]}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Developer Guide') }}</label><span class="text-danger">*</span>
                            <br>
                            <span class="small"></span>
                            <input class="form-control"  name="asset_url" type="file" id="asset_url" value="{{old('asset_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('User Guide Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="doc_base_url" type="file" id="doc_base_url" value="{{old('doc_base_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Wireframe (Draw.io)') }}</label><span class="text-danger"> (if any) </span>
                            <input class="form-control"  name="wireframe_url" type="file" id="wireframe_url" value="{{old('wireframe_url')}}">
                        </div>


                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project API EndPoints Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="api_endpoints_url" type="text" id="doc_base_url" value="{{old('api_endpoints_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Dev Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="dev_url" type="text" id="dev_url" value="{{old('dev_url')}}">
                        </div>

                        <div class="form-group">
                            <label for="example-date-input" class="form-control-label">{{ __('Date') }}</label>
                            <input class="form-control" name="date" type="date" value="{{date('Y-m-d')}}" id="date">
                        </div>

                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn bg-gradient-secondary">{{ __('Save') }}</button>
                        <button type="button" class="btn bg-gradient-primary">{{ __('Close') }}</button>
                    </form>

                </div>

            </div>
        </div>


            <div class="col-md-6">
                <div class="card">
                    <div class="card-header p-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h6 class="mb-0">{{ __('Project list') }}</h6>
                            </div>

                        </div>
                        <hr class="horizontal dark mb-0">
                    </div>
                    <div class="card-body p-3 pt-0">
                        <ul class="list-group list-group-flush" data-toggle="checklist">
                            @foreach($projects as $project)
                                <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                                    <div class="checklist-item checklist-item-primary ps-2 ms-3">
                                        <div class="d-flex align-items-center">


                                            <div class="form-check">
                                                <input class="form-check-input todo_checkbox" type="checkbox"
                                                       data-id=""
                                                >

                                            </div>
                                            <h6 class="mb-0 text-dark font-weight-bold text-sm">{{$project->name}}</h6>
                                            <div class="dropdown float-lg-end ms-auto pe-4">
                                                <a href="javascript:;" class="cursor-pointer" id="dropdownTable2" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>Delete</a>
                                                    <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                                </a>

                                            </div>
                                        </div>
                                        <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                                            <div>
                                                <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                                                <span class="text-xs font-weight-bolder"></span>
                                            </div>

                                        </div>
                                    </div>
                                    <hr class="horizontal dark mt-4 mb-0">
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

            </div>
    </div>

{{--    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">--}}
{{--        <div class="modal-dialog modal-lg">--}}
{{--            <div class="modal-content">--}}
{{--                <div id="dropzone">--}}
{{--                    <form action="{{ route('dropzoneFileUpload') }}" class="dropzone" id="file-upload" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="dz-message">--}}
{{--                            Drag and Drop Single/Multiple Files Here<br>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--                <div class="container">--}}
{{--                <div class="form-group">--}}
{{--                    <form>--}}
{{--                        @csrf--}}
{{--                        <label for="example-text-input" class="form-control-label">{{ __('Upload') }}</label><span class="text-danger">*</span>--}}
{{--                        <input type="file" name="doc_base_url" id="doc_base_url" class="form-control" />--}}

{{--                        <button type="button" class="btn btn-success upload-file">Upload</button>--}}
{{--                    </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


@endsection


@section('script')

    <script>

        $(".upload-file").on("click",function (){
            var input = document.querySelector('input[type="file"]')

            var data = new FormData()
            data.append('file', input.files[0])
            data.append('user', 'hubot')

            fetch("{{route('dropzoneFileUpload')}}", {
                method: 'POST',
                body: data
            })
        })

    </script>


    <script>
        "use strict";
        $(function () {


            flatpickr("#date", {

                dateFormat: "Y-m-d",
            });

        });




    </script>

    <script>
        tinymce.init({
            selector: '#description',


            plugins: 'table,code',


        });
    </script>
    <script>
        $(function () {
            $('.todo_checkbox').on('change',function () {
                let that = $(this);
                if(this.checked)
                {
                   $.post('/todos/change-status',{
                       id: that.attr('data-id'),
                       status: 'Completed',
                       _token: '{{csrf_token()}}',
                   });
                }
                else{
                    $.post('/todos/change-status',{
                        id: that.attr('data-id'),
                        status: 'Not Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
            });
        });
    </script>

    <script>
        $(".select-skills").select2({
            placeholder: 'Type skill name...',
            minimumInputLength: 3,
            theme: "classic",
            allowClear: true,
            ajax: {
                url: '/api/skills',
                dataType: 'json',
                data: function(params) {
                    return {
                        q: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

    </script>

    <script>
        const submitBtn = $("#submitBtn");

        function disableButton(){
            submitBtn.html("Uploading...");
            setTimeout(function (){
                submitBtn.prop("disabled",true);
            },500)
        }
    </script>



@endsection




