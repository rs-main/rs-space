@extends('layouts.primary')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <form action="{{route('modules.store')}}" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger" style="color: white;">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-lg-9 col-12 mx-auto">
                    <div class="card card-body mt-4">
                        <h6 class="mb-0">{{__('New Module')}}</h6>
                        <p class="text-sm mb-0">{{__('Create new module')}}</p>
                        <hr class="horizontal dark my-3">


                        <label for="projectName" class="form-label">{{__('Module Name')}}</label>
                        <input type="text" value="{{old("name")}}"  name="name" class="form-control" id="projectName">
                        <br>

                        <label for="projectName" class="form-label">{{__('Category')}}</label>
                        <select class="form-control" name="category_id">
                            <option value="">Select Category </option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>

                        <br>
                        <label for="projectName" class="form-label">{{__('Project')}}</label>
                        <select class="form-control" name="project_id">
                            <option value="">Select Project </option>
                            @foreach($selectProjects as $selectProject)
                                <option @if($project_id == $selectProject->id ) selected @endif
                                value="{{$selectProject->id}}">{{$selectProject->name}}</option>
                            @endforeach
                        </select>


                        <label class="mt-4 text-sm mb-0">{{__('Project Summary')}}</label>
                        <p class="form-text text-muted text-xs ms-1">
                            {{__('Write a short summary of the module.Within 225 words')}}
                        </p>

                        <div class="form-group">
                            <textarea name="summary" class="form-control" rows="4" id="editor" name="budget">{{old('summary')}}</textarea>
                        </div>


                        <div class="row mt-4">
                            <div class="col-6">
                                <label class="form-label">{{__('Start Date')}}</label>
                                <input class="form-control datetimepicker" name="start_date" type="date" id="start_date" placeholder="Please select start date"
                                       value="{{old("start_date")}}" />
                            </div>
                            <div class="col-6">
                                <label class="form-label">{{__('End Date')}}</label>
                                <input class="form-control datetimepicker" name="end_date" type="date" id="end_date"  placeholder="Please select end date"
                                 value="{{old('end_date')}}">
                            </div>
                        </div>


                        <label class="mt-4 text-sm mb-0">{{__('Module Description')}}</label>
                        <p class="form-text text-muted text-xs ms-1">
                            {{__('Write a well organised description of the module.')}}
                        </p>

                        <div class="form-group">
                            <textarea class="form-control" rows="10" id="description" name="description">{{old('description')}}
                            </textarea>
                        </div>

                        <div class="row mt-3 mb-4">
                            <div class="col-12">
                                <label class="text-sm mb-0">{{__('Stack Required')}}</label>
                                <select class="select-skills" name="skills[]" multiple="multiple" style="width: 100%; height: 120px"></select>
                                {{--                                                <input  name="skills" class="multisteps-form__input form-control" type="text" @if(!empty($selected_user)) value="{{$selected_user->twitter}}" @endif />--}}
                            </div>

                        </div>

                        <div class="row mt-3 mb-4">
                            <div class="col-12">
                                <label class="text-sm mb-0">{{__('Private')}}</label>
                                <input type="checkbox" name="private" />
{{--                                <select class="select-skills" name="skills[]" multiple="multiple" style="width: 100%; height: 120px"></select>--}}
                                {{--                                                <input  name="skills" class="multisteps-form__input form-control" type="text" @if(!empty($selected_user)) value="{{$selected_user->twitter}}" @endif />--}}
                            </div>

                        </div>

                        <input type="hidden" name="staff" value="{{Request::query("staff")}}" />

                        <div class="d-flex  mt-4">

                            <button type="submit" name="button" class="btn bg-gradient-primary m-0 ">
                                {{__('Save')}}
                            </button>
                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>
@endsection

@section('script')

    <script>
        "use strict";
        $(function () {


            flatpickr("#start_date", {

                dateFormat: "Y-m-d",
            });

            flatpickr("#end_date", {

                dateFormat: "Y-m-d",
            });


            tinymce.init({
                selector: '#description',


                plugins: 'table,code',


            });

        });


    </script>

    <script>
        $(".select-skills").select2({
            placeholder: 'Type skill name...',
            minimumInputLength: 3,
            theme: "classic",
            allowClear: true,
            ajax: {
                url: '/api/skills',
                dataType: 'json',
                data: function(params) {
                    return {
                        q: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

    </script>


@endsection

