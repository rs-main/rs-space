@extends("layouts.primary")

@section("content")

<div class="container-fluid py-4">
    <div class="col-lg-9 mx-auto">
        <div class="card card-body ">
            <div class="page-header mb-4 border-radius-xl">
                <span class="mask bg-gradient-dark"></span>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 mt-3 mb-6 text-center my-auto">
                            <h3 class="text-white">
                                {{$module->name}} Documentation
                            </h3>
                            <h2 class="text-white fadeIn2 fadeInBottom mt-4">{{$module->name}}</h2>
                            <h5 class="text-white fadeIn2 fadeInBottom">{{$module->start_date}} - {{$module->end_date}}</h5>
                        </div>
                    </div>
                </div>
            </div>

            @foreach($tasks as $task)

            <div class="text-center mt-4">
                <h6>{{$task->name}}</h6>
            </div>
            <div>
                <p class="pl-5">
                    {!! $task->userTask? $task->userTask->documentation : "" !!}
                </p>
            </div>
            @endforeach

        </div>
    </div>
</div>

@endsection
