@php
    $admin = Auth::user()->admin;
@endphp
@extends('layouts.primary')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <form action="{{route('modules.update',$module->id)}}" method="post">
                @method('PATCH')
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger" style="color: white;">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-lg-9 col-12 mx-auto">
                    <div class="card card-body mt-4">
                        @if(Auth::user()->admin)
                            <h6 class="mb-0">{{__('Edit Module')}}</h6>
                            <p class="text-sm mb-0">{{__('Edit module')}}</p>
                            <hr class="horizontal dark my-3">

                        @else
                            <h6 class="mb-0">{{__('View Module')}}</h6>
                            <p class="text-sm mb-0">{{__(' module')}}</p>
                            <hr class="horizontal dark my-3">
                        @endif


                        <label for="projectName" class="form-label">{{__('Module Name')}}</label>
                        <input type="text" value="{{$module->name}}"  name="name" class="form-control" id="projectName" @if(!$admin) disabled @endif  >
                        <br>

                        <label for="projectName" class="form-label">{{__('Project')}}</label>
                        <select class="form-control" name="project_id" @if(!$admin) disabled @endif>
                            @foreach($selectProjects as $selectProject)
                                <option value="{{$selectProject->id}}">{{$selectProject->name}}</option>
                            @endforeach
                        </select>

                        <label class="mt-4 text-sm mb-0">{{__('Project Summary')}}</label>
                        <p class="form-text text-muted text-xs ms-1">
                            {{__('Write a short summary of the module.Within 225 words')}}
                        </p>

                        <div class="form-group">
                            <textarea name="summary" class="form-control" rows="4" id="editor" name="budget" @if(!$admin) disabled @endif >{{$module->summary}}</textarea>
                        </div>

                        <div class="row mt-4">
                            <div class="col-6">
                                <label class="form-label">{{__('Start Date')}}</label>
                                <input class="form-control datetimepicker" name="start_date" type="date" id="start_date" placeholder="Please select start date"
                                       value="{{$module->start_date}}" @if(!$admin) disabled @endif />
                            </div>
                            <div class="col-6">
                                <label class="form-label">{{__('End Date')}}</label>
                                <input class="form-control datetimepicker" name="end_date" type="date" id="end_date"  placeholder="Please select end date"
                                       value="{{$module->end_date}}" @if(!$admin) disabled @endif >
                            </div>
                        </div>


                        <label class="mt-4 text-sm mb-0">{{__('Module Description')}}</label>
                        <p class="form-text text-muted text-xs ms-1">
                            {{__('Write a well organised description of the module.')}}
                        </p>

                        <div class="form-group">
                            <textarea  class="form-control" rows="10" id="description" name="description" >
                                {{$module->description}}
                            </textarea>
                        </div>

                        <div class="row mt-3 mb-4">
                                <div class="col-12">
                                    <label class="text-sm mb-0">{{__('Private')}}</label>
                                    <input type="checkbox" name="private" @if($module->private) checked @endif />
                                </div>

                            </div>

                        @if($admin)
                            <div class="d-flex  mt-4">

                                <button type="submit" class="btn bg-gradient-primary m-0 ">
                                    {{__('UPDATE')}}
                                </button>

                            </div>
                        @endif

                    </div>
                </div>

            </form>

        </div>

    </div>
@endsection

@section('script')

    <script>

        let status = "{{$admin}}" == 1 ? 0 : 1;
        "use strict";
        $(function () {


            flatpickr("#start_date", {

                dateFormat: "Y-m-d",
            });

            flatpickr("#end_date", {

                dateFormat: "Y-m-d",
            });


            tinymce.init({
                selector: '#description',
                plugins: 'table,code',
                readonly: status

            });

        });


    </script>

@endsection

