@php
    $admin = Auth::user()->admin;
@endphp

@extends('layouts.primary')
@section('content')

    <style>
        .chip{
            width: auto;
            color: white;
            background-color: #2F3459;
            border-radius: 7px;
            margin-right: 5px;
            margin-top: 5px;

        }
    </style>


    <div class="page-header mb-4 border-radius-xl">
        <span class="mask bg-gradient-dark"></span>
        <div class="container">
            <div class="row">

                <div class="col-lg-6 my-auto">
                    <h5 class="text-white fadeIn2 fadeInBottom mt-4">
                        {{__($quote->text)}}
                    </h5>
                    <p class="text-white opacity-8 fadeIn3 fadeInBottom">{{__($quote->author)}} </p>
                </div>
            </div>
            @if($admin)
            <a  href="{{$create_url}}" type="button" class="btn bg-gradient-light">{{__('Create Module')}}</a>
            @endif
        </div>
    </div>


    <div class="container-fluid py-4">
        <section class="py-3">
            <div class="row">
                <div class="col-md-8 me-auto text-left">
                    <h5>{{__('List of Modules')}}</h5>

                </div>
            </div>
            <div class="row mt-lg-4 mt-2">
                @foreach($modules as $module)

                    <div class="col-lg-4 col-md-6 mb-4">
                        @if(Request::has("staff"))

                        <div class="card card-blog" >
                        @else
                        <div class="card card-blog ">
                        @endif
                            <div class="card-body p-3">
                                <div class="d-flex">
                                    <div class="avatar avatar-xl bg-gradient-dark border-radius-md p-2">
                                        <h1 class="text-white">{{$module->name['0']}}</h1>
                                    </div>
                                    <div class="ms-3 my-auto">
                                        <h6>{{$module->name}}</h6>
                                    </div>
                                </div>

                                <p class="text-sm mt-3"> {{$module->summary}} </p>
                                <span class="badge bg-gradient-success font-weight-bold">{{$module->status}}</span>

                                @if($module->end_date < \Carbon\Carbon::now()->toDateString())
                                    <span class="badge bg-gradient-danger font-weight-bold">
                                        Overdue
                                    </span>
                                @endif


                                <hr class="horizontal dark">
                                <div class="row">
                                    <div class="col-6 ">
                                        <h6 class="text-sm mb-0">{{$module->start_date}}</h6>
                                        <p class="text-secondary text-sm font-weight-bold mb-0">{{__('Start date')}}</p>
                                    </div>
                                    <div class="col-6 text-end">
                                        <h6 class="text-sm mb-0">{{$module->end_date}}</h6>
                                        <p class="text-secondary text-sm font-weight-bold mb-0">{{__('Due date')}}</p>
                                    </div>
                                    <div class="btn-group">

                                        @if(Request::has("staff"))

                                            @if(\App\Models\UserModule::whereUserId(Request::query("staff"))->whereModuleId($module->id)->first())
                                            <a href="#"  type="button" class="btn bg-gradient-dark mt-3 unassign-module" data-id="{{$module->id}}">{{__('Assigned')}}</a>
                                            @else
                                                <form action="{{route('assign-module')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="module_id" value="{{$module->id}}"/>
                                                    <input type="hidden" name="user_id" value="{{Request::query('staff')}}"/>
                                                    <button type="submit" class="btn bg-gradient-dark mt-3">{{__('Assign Module')}}</button>
                                                </form>
                                            @endif
                                        @else

                                            <a href="{{route('tasks.index',['module_id' => $module->id])}}" type="button" class="btn bg-gradient-dark mt-3">{{__('View')}}</a>


                                            <a href="{{route('modules.edit',$module->id)}}" type="button" class="btn bg-gradient-secondary mt-3">
                                                @if(!$admin) {{__('Read')}} @else {{__('Edit')}}@endif
                                            </a>

                                            <a href="{{route("module-documentation",$module->id)}}" type="button" class="btn bg-gradient-primary mt-3">{{__('Documentation')}}</a>
                                        @endif

                                    </div>

                                    <div class="row m-lg-3">

                                        @foreach($module->skills as $skill)
                                        <div class="chip">
                                            {{$skill->skill->name}}
                                        </div>
                                        @endforeach
                                    </div>

                                    <div class="col-12 ">

                                        <div class="progress" style="height: 20px">
                                            <div class="progress-bar" role="progressbar"
                                                 style="width: {{\App\Models\Task::PointsPercentage($module->id)}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                            </div>

                                        </div>

                                        <p class="text-secondary text-sm font-weight-bold mb-0">{{__('Completed Status')}}</p>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                @endforeach


                @if($admin)
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <div class="card-body d-flex flex-column justify-content-center text-center">
                            <a href="{{$create_url}}">
                                <i class="fa fa-plus text-secondary mb-3"></i>
                                <h5 class=" text-secondary">{{__('New Module')}} </h5>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            </div>
        </section>

    </div>

@endsection

@section('script')
    <script>
        $(".unassign-module").on("click",function (){
            let id = $(this).data("id");
            let action = confirm("Are you sure you want to unconfirm?")
            if (action){
                alert("module-"+id);
            }
        })
    </script>
@endsection



