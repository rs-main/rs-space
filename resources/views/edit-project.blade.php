@extends('layouts.primary')
@section('content')



    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    {{ __('  Edit Project') }}

                    <a href="/dashboard" type="submit" class="btn bg-gradient-secondary float-end">{{ __('Go to Projects') }}</a>
                </div>

                <div class="card-body">
                    <form action="{{route('update-project',$project->id)}}" method="post">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger" style="color: white">
                                <ul class="list-unstyled">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Name') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="name" type="text" id="title" value="{{$project->name}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1">{{ __('Description') }}</label><span class="text-danger">*</span>
                            <textarea class="form-control" name="description" id="description" rows="3" > {{$project->description}} </textarea>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Repository Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="repository_url" type="text" id="repository_url" value="{{$project->repository_url}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Asset Url(s)') }}</label><span class="text-danger">*</span>
                            <span class="small"> Separate urls with a comma(,)</span>
<!--                            <input class="form-control"  name="asset_url" type="text" id="asset_url">-->
                            <textarea class="form-control" name="asset_url">{{$project->asset_url}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Doc Base Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="doc_base_url" type="text" id="doc_base_url" value="{{$project->doc_base_url}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project API EndPoints Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="api_endpoints_url" type="text" id="api_endpoints_url" value="{{$project->api_endpoints_url}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Project Dev Url') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="dev_url" type="text" id="dev_url" value="{{$project->dev_url}}">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('Wireframe Url') }}</label><span class="text-danger"> (if any) </span>
                            <input class="form-control"  name="wireframe_url" type="text" id="title" value="{{$project->wireframe_url}}">
                        </div>

<!--                        <div class="form-group">-->
<!--                            <label for="example-date-input" class="form-control-label">{{ __('Date') }}</label>-->
<!--                            <input class="form-control" name="date" type="date" value="{{$project->created_at}}" id="date">-->
<!--                        </div>-->


                            <input type="hidden" name="id" value="">

                        <button type="submit" class="btn bg-gradient-secondary">{{ __('Update') }}</button>
                        <button type="button" class="btn bg-gradient-primary">{{ __('Close') }}</button>
                    </form>

                </div>

            </div>
        </div>




            <div class="col-md-7">
                <div class="card">
                    <div class="card-header p-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h6 class="mb-0">{{ __('Project list') }}</h6>
                            </div>

                        </div>
                        <hr class="horizontal dark mb-0">
                    </div>
                    <div class="card-body p-3 pt-0">
                        <ul class="list-group list-group-flush" data-toggle="checklist">
                            @foreach($projects as $project)
                                <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                                    <div class="checklist-item checklist-item-primary ps-2 ms-3">
                                        <div class="d-flex align-items-center">


                                            <div class="form-check">
                                                <input class="form-check-input todo_checkbox" type="checkbox"
                                                       data-id=""
                                                >

                                            </div>
                                            <h6 class="mb-0 text-dark font-weight-bold text-sm">{{$project->name}}</h6>
                                            <div class="dropdown float-lg-end ms-auto pe-4">
                                                <a href="javascript:;" class="cursor-pointer" id="dropdownTable2" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>Delete</a>
                                                    <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                                </a>

                                            </div>
                                        </div>
                                        <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                                            <div>
                                                <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                                                <span class="text-xs font-weight-bolder"></span>
                                            </div>

                                        </div>
                                    </div>
                                    <hr class="horizontal dark mt-4 mb-0">
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

            </div>
    </div>


@endsection


@section('script')


    <script>
        "use strict";
        $(function () {


            flatpickr("#date", {

                dateFormat: "Y-m-d",
            });

        });




    </script>

    <script>
        tinymce.init({
            selector: '#description',


            plugins: 'table,code',


        });
    </script>
    <script>
        $(function () {
            $('.todo_checkbox').on('change',function () {
                let that = $(this);
                if(this.checked)
                {
                   $.post('/todos/change-status',{
                       id: that.attr('data-id'),
                       status: 'Completed',
                       _token: '{{csrf_token()}}',
                   });
                }
                else{
                    $.post('/todos/change-status',{
                        id: that.attr('data-id'),
                        status: 'Not Completed',
                        _token: '{{csrf_token()}}',
                    });
                }
            });
        });
    </script>


@endsection




