@extends('layouts.primary')

@section('head')
    {{--    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js"></script>


@endsection

@section('content')

    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    {{ __('  Add New File') }}

                    <a href="/dashboard" type="submit" class="btn bg-gradient-secondary float-end">{{ __('Go to Projects') }}</a>
                </div>

                <div class="card-body">
                    <form action="{{route('dropzoneFileUpload')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger" style="color: white">
                                <ul class="list-unstyled">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


{{--                        <div class="form-group">--}}
{{--                            <label for="type" class="form-control-label">{{ __('Type') }}</label><span class="text-danger">*</span>--}}
{{--                            <select class="form-control" name="type">--}}
{{--                                <option value="">Select type</option>--}}
{{--                                <option value="text/html">Text/Html</option>--}}
{{--                                <option value="application/pdf">PDF</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}

                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">{{ __('File') }}</label><span class="text-danger">*</span>
                            <input class="form-control"  name="file" type="file" id="title" value="{{old('file')}}">
                        </div>

                        <button id="submitBtn" type="submit" class="btn bg-gradient-secondary" onclick="disableButton()">{{ __('Save') }}</button>
                        <button type="button" class="btn bg-gradient-primary">{{ __('Close') }}</button>
                    </form>

                </div>

            </div>
        </div>


        <div class="col-md-7">
            <div class="card">
                <div class="card-header p-3">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mb-0">{{ __('Recent Uploads') }}</h6>
                        </div>

                    </div>
                    <hr class="horizontal dark mb-0">
                </div>
                <div class="card-body p-3 pt-0">
                    <ul class="list-group list-group-flush" data-toggle="checklist">
                        @foreach($s3folders as $folder)
                            <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                                <div class="checklist-item checklist-item-primary ps-2 ms-3">
                                    <div class="d-flex align-items-center">


                                        <div class="form-check">
                                            <input class="form-check-input todo_checkbox" type="checkbox"
                                                   data-id=""
                                            >

                                        </div>
                                        <h6 class="mb-0 text-dark font-weight-bold text-sm">{{$folder}}</h6>
                                        <div class="dropdown float-lg-end ms-auto pe-4">
                                            <a href="javascript:;" class="cursor-pointer" id="dropdownTable2" data-bs-toggle="dropdown" aria-expanded="false">
                                                <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="#"><i class="far fa-trash-alt me-2"></i>Delete</a>
                                                <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                                        <div>
                                            <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                                            <span class="text-xs font-weight-bolder"></span>
                                        </div>

                                    </div>
                                </div>
                                <hr class="horizontal dark mt-4 mb-0">
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>

        </div>
    </div>

@endsection


@section('script')

    <script>
        const submitBtn = $("#submitBtn");

        function disableButton(){
                submitBtn.html("Uploading...");
                setTimeout(function (){
                    submitBtn.prop("disabled",true);
                },500)
        }
    </script>

@endsection




