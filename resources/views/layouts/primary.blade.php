@php
    $jobs_url = Auth::user()->admin ? route("jobs-dashboard") : route("jobs.index");
@endphp

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
        RS Space
    </title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link id="pagestyle" href="/css/app.css?v=53" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
        integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
        crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


@yield('head')

</head>

<body class="g-sidenav-show   bg-gray-100">
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-left ms-3" id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute right-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{config('app.url')}}/dashboard">
            @if(!empty($settings ?? ''['logo']))
                <img src="" class="navbar-brand-img h-100" alt="...">
<!--                <img src="{{asset($settings ?? ''['logo'])}}" class="navbar-brand-img h-100" alt="...">-->
            @endif
            <span class="ms-1 font-weight-bold"> Real Studios Online </span>
        </a>
    </div>
    <hr class="horizontal dark mt-0">

    <div class="collapse navbar-collapse  w-auto" id="sidenav-collapse-main">


        <ul class="navbar-nav">

            <li class="nav-item">
                <a class="nav-link @if(($selected_navigation ?? '') === 'dashboard') active @endif" href="/dashboard">
                    <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">

                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                            <path class="color-background" fill-rule="evenodd" d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                            <path class="color-background" fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                        </svg>

                    </div>
                    <span class="nav-link-text ms-1">{{ __('Dashboard') }}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link @if(($selected_navigation ?? '') === 'dashboard') active @endif" href="{{route("modules.index")}}">
                    <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">

                        <svg height="15px" version="1.1" viewBox="0 0 19 15" width="19px" xmlns="http://www.w3.org/2000/svg"
                             xmlns:sketch="http://www.bohemiancoding.com/sketch/ns"
                             xmlns:xlink="http://www.w3.org/1999/xlink"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1">
                                <g fill="#000000" id="Core" transform="translate(-129.000000, -509.000000)"><g id="view-module" transform="translate(129.500000, 509.500000)">
                                        <path d="M0,6 L5,6 L5,0 L0,0 L0,6 L0,6 Z M0,13 L5,13 L5,7 L0,7 L0,13 L0,13 Z M6,13 L11,13 L11,7 L6,7 L6,13 L6,13 Z M12,13 L17,13 L17,7 L12,7 L12,13 L12,13 Z M6,6 L11,6 L11,0 L6,0 L6,6 L6,6 Z M12,0 L12,6 L17,6 L17,0 L12,0 L12,0 Z" id="Shape"/>
                                </g></g></g></svg>
                    </div>
                    <span class="nav-link-text ms-1">{{ __('Modules') }}</span>
                </a>
            </li>

            @if(Auth::user()->admin)

             <li class="nav-item">
                <a class="nav-link @if(($selected_navigation ?? '') === 'jobs-dashboard') active @endif"
                   href="{{route('jobs-dashboard')}}">
                    <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">

                        <svg enable-background="new 0 0 64 64" id="Layer_1" version="1.1" viewBox="0 0 64 64" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M51,49V28c0-1.4,1.1-2.5,2.5-2.5S56,26.6,56,28v21.8c0,2.5-2.7,5.2-5.1,5.2H13.6C11.1,55,9,52.3,9,49.8V12.3  C9,10,10.9,8,13.1,8h27.7c2.5,0,5.2,1.8,5.2,4.3v37.2c0,0-0.2,5.5,4.9,5.5" fill="#E5E5E5"/><g><path d="M51,28c0-1.4,1.1-2.5,2.5-2.5H46v24c0,0-0.2,5.5,4.9,5.5l0.1-6V28z" fill="#B7B7B7"/><path d="M53.5,55" fill="#E5E5E5"/></g><path d="  M29,14.7v4.1V23c0,0,4.4,0.6,4.4-1.6c0-2.8-2.8-2.1-3.5-2.4c0.7-0.2,2.8-0.2,2.8-2.1C32.7,14.4,29,14.7,29,14.7z" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M26,20.5c0,1.4-1.1,2.5-2.5,2.5S21,21.9,21,20.5v-3.3c0-1.4,1.1-2.5,2.5-2.5s2.5,1.1,2.5,2.5V20.5z" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M13.6,21.5c0.4,0.9,1.1,1.6,2.1,1.6c1.4,0,2.3-1.1,2.3-2.5V15h-2" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M51,49V28c0-1.4,1.1-2.5,2.5-2.5S56,26.6,56,28v21.8c0,2.5-2.7,5.2-5.1,5.2H13.6C11.1,55,9,52.3,9,49.8V12.3C9,10,10.9,8,13.1,8  h27.7c2.5,0,5.2,1.8,5.2,4.3v37.2c0,0-0.2,5.5,4.9,5.5" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M40.8,17.2c0-1.4-1.1-2.5-2.4-2.5c-1.4,0-2.4,1.1-2.4,2.5c0,0.9,4.9,2,4.9,3.3c0,1.4-1.1,2.5-2.4,2.5c-1.4,0-2.4-1.1-2.4-2.5" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="21" y1="28" y2="28"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="25" y1="31" y2="31"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="24" y1="34" y2="34"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="36" y1="43" y2="43"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="40" y1="46" y2="46"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="39" y1="49" y2="49"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="37" y1="28" y2="28"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="40" y1="31" y2="31"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="40" y1="37" y2="37"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="39" y1="34" y2="34"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="22" y1="40" y2="40"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="25" y1="43" y2="43"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="25" y1="49" y2="49"/><line fill="none" stroke="#2C3E50" stroke-linecap="round"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="24" y1="46" y2="46"/></svg>
                    </div>
                    <span class="nav-link-text ms-1">{{ __('Jobs Applied') }}</span>
                </a>
            </li>

            @endif


            <li class="nav-item">
                <a class="nav-link @if(($selected_navigation ?? '') === 'jobs-dashboard') active @endif"
                   href="{{route("jobs.index")}}">
                    <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">

                        <svg enable-background="new 0 0 64 64" id="Layer_1" version="1.1" viewBox="0 0 64 64" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M51,49V28c0-1.4,1.1-2.5,2.5-2.5S56,26.6,56,28v21.8c0,2.5-2.7,5.2-5.1,5.2H13.6C11.1,55,9,52.3,9,49.8V12.3  C9,10,10.9,8,13.1,8h27.7c2.5,0,5.2,1.8,5.2,4.3v37.2c0,0-0.2,5.5,4.9,5.5" fill="#E5E5E5"/><g><path d="M51,28c0-1.4,1.1-2.5,2.5-2.5H46v24c0,0-0.2,5.5,4.9,5.5l0.1-6V28z" fill="#B7B7B7"/><path d="M53.5,55" fill="#E5E5E5"/></g><path d="  M29,14.7v4.1V23c0,0,4.4,0.6,4.4-1.6c0-2.8-2.8-2.1-3.5-2.4c0.7-0.2,2.8-0.2,2.8-2.1C32.7,14.4,29,14.7,29,14.7z" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M26,20.5c0,1.4-1.1,2.5-2.5,2.5S21,21.9,21,20.5v-3.3c0-1.4,1.1-2.5,2.5-2.5s2.5,1.1,2.5,2.5V20.5z" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M13.6,21.5c0.4,0.9,1.1,1.6,2.1,1.6c1.4,0,2.3-1.1,2.3-2.5V15h-2" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M51,49V28c0-1.4,1.1-2.5,2.5-2.5S56,26.6,56,28v21.8c0,2.5-2.7,5.2-5.1,5.2H13.6C11.1,55,9,52.3,9,49.8V12.3C9,10,10.9,8,13.1,8  h27.7c2.5,0,5.2,1.8,5.2,4.3v37.2c0,0-0.2,5.5,4.9,5.5" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><path d="  M40.8,17.2c0-1.4-1.1-2.5-2.4-2.5c-1.4,0-2.4,1.1-2.4,2.5c0,0.9,4.9,2,4.9,3.3c0,1.4-1.1,2.5-2.4,2.5c-1.4,0-2.4-1.1-2.4-2.5" fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="21" y1="28" y2="28"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="25" y1="31" y2="31"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="24" y1="34" y2="34"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="36" y1="43" y2="43"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="40" y1="46" y2="46"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="39" y1="49" y2="49"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="37" y1="28" y2="28"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="40" y1="31" y2="31"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="40" y1="37" y2="37"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="30" x2="39" y1="34" y2="34"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="22" y1="40" y2="40"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="25" y1="43" y2="43"/><line fill="none" stroke="#2C3E50" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="25" y1="49" y2="49"/><line fill="none" stroke="#2C3E50" stroke-linecap="round"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="15" x2="24" y1="46" y2="46"/></svg>
                    </div>
                    <span class="nav-link-text ms-1">{{ __('Job Board') }}</span>
                </a>
            </li>


            @if(Auth::user()->admin)

                <li class="nav-item">
                    <a class="nav-link @if(($selected_navigation ?? '') === 'staff') active @endif " href="/staff">
                        <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">


                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                <path class="color-background" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            </svg>
                        </div>
                        <span class="nav-link-text ms-1">{{__('Staff')}}</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('fileUploads')}}">
                        <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                            <svg enable-background="new 0 0 32 32" height="16px" version="1.1" viewBox="0 0 32 32" width="16px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="upload_laptop_arrow"><g><g id="laptop_3_"><g><g><g><g><path d="M3.5,26C3.224,26,3,25.776,3,25.5v-16C3,8.673,3.673,8,4.5,8h5.001c0.276,0,0.5,0.224,0.5,0.5         S9.777,9,9.501,9H4.5C4.225,9,4,9.225,4,9.5v16C4,25.776,3.776,26,3.5,26z" fill="#263238"/><path d="M28.5,26c-0.276,0-0.5-0.224-0.5-0.5v-16C28,9.225,27.775,9,27.5,9h-5C22.224,9,22,8.776,22,8.5         S22.224,8,22.5,8h5C28.327,8,29,8.673,29,9.5v16C29,25.776,28.776,26,28.5,26z" fill="#263238"/></g></g></g><g><g><path d="M28.5,30h-25C2.121,30,1,28.879,1,27.5C1,27.224,1.224,27,1.5,27h11c0.276,0,0.5,0.224,0.5,0.5        S12.776,28,12.5,28H2.086c0.206,0.582,0.762,1,1.414,1h25c0.652,0,1.208-0.418,1.414-1H19.5c-0.276,0-0.5-0.224-0.5-0.5        s0.224-0.5,0.5-0.5h11c0.276,0,0.5,0.224,0.5,0.5C31,28.879,29.879,30,28.5,30z" fill="#263238"/></g></g></g></g></g><g><g id="transfer_9_"><g><path d="M13.502,20c-0.276,0-0.5-0.224-0.5-0.5V9h-1.5c-0.183,0-0.352-0.1-0.438-0.261      c-0.088-0.16-0.081-0.355,0.018-0.51l4.5-7c0.186-0.285,0.656-0.285,0.842,0l4.5,7c0.099,0.154,0.105,0.35,0.018,0.51      C20.854,8.9,20.685,9,20.502,9h-1.5v8.5c0,0.276-0.224,0.5-0.5,0.5s-0.5-0.224-0.5-0.5v-9c0-0.276,0.224-0.5,0.5-0.5h1.084      l-3.584-5.575L12.418,8h1.084c0.276,0,0.5,0.224,0.5,0.5v11C14.002,19.776,13.778,20,13.502,20z" fill="#263238"/></g></g><circle cx="18.5" cy="19.5" fill="#263238" r="0.5"/></g></g></svg>
                        </div>
                        <span class="nav-link-text ms-1">{{__('Uploads')}}</span>
                    </a>
                </li>
            @endif


        </ul>
    </div>

</aside>
<main class="main-content mt-1 border-radius-lg">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">

                <h6 class="font-weight-bolder mb-0"></h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    <div class="input-group">

                    </div>
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <a href="/profile" class="nav-link text-body font-weight-bold px-0">
                            <i class="fa fa-user me-sm-1"></i>
                            @if(Auth::check())
                            <span class="d-sm-inline d-none"> {{Auth::user()->first_name}} {{Auth::user()->last_name}}</span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item px-3 d-flex align-items-center">
                        <a href="/logout" class="nav-link text-body p-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right-square-fill" viewBox="0 0 16 16">
                                <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z"/>
                            </svg>
                        </a>
                    </li>
                    <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
                        <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </a>
                    </li>



                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        @include('flash-message')
        @yield('content')
    </div>
</main>

</div>
<!--   Core JS Files   -->
<script src="/js/app.js?v=53"></script>
<script src="/lib/tinymce/tinymce.min.js?v=50"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>
    "use strict"
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
</script>

@yield('script')

</body>

</html>

