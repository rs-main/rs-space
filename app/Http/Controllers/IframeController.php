<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IframeController extends Controller
{
    //

    public function view(){
        $url = \request()->query("url");
        return view("viewer.index",[
            "url" => $url
        ]);
    }

}
