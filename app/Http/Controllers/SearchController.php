<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;
use MeiliSearch\Client;

class SearchController extends Controller
{

    public function search(Request $request){

//        $client = new Client('localhost:7700', 'masterKey');
//
//# An index is where the documents are stored.
//        $index = $client->index('skills');
//
//# Then, you can start searching
//        return $hits = $index->search('java')->getHits();

        return Skill::search('java')->get();

    }
}
