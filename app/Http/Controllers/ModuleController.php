<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Module;
use App\Models\ModuleSkill;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Models\UserModule;
use App\Notifications\AssignModuleNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModuleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $project_id = \request()->query('project_id');
        $staff_id   = \request()->query('staff');
        $create_url = "/modules/create";

        if (\request()->has('project_id')) {
            $modules = Module::with(["skills" => function($skills){
                return $skills->with("skill")->get();
            }])->whereProjectId($project_id)->get();
            $create_url = route('modules.create',["project_id" => $project_id]);
        }

        else{
             $modules = Module::with(["skills" => function($skills){
                return $skills->with("skill:id,name")->get();
            }])->get();
        }

        if (\request()->has('staff')) {
            if (\request()->has("page") and \request()->query("page") == "projects") {
                if (Auth::user()->admin) {
                    $modules = Module::with(["skills" => function ($skills) {
                        return $skills->with("skill:id,name")->get();
                    }]);
                } else {
                    $userModuleIds = UserModule::whereUserId($staff_id)->pluck("module_id");
                    $modules = Module::with(["skills" => function ($skills) {
                        return $skills->with("skill:id,name")->get();
                    }])->whereIn("id", $userModuleIds)->get();
                };
            }
            $create_url = route('modules.create',["staff" => $staff_id]);
        }

        return view('modules.index', [
            "modules" => $modules,
            "settings" => [],
            "create_url" => $create_url,
            "project_id" => $project_id
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $project = false;
        $project_id = \request()->query('project_id');

        if($request->id)
        {
            $project = Module::find($request->id);
        }

        $selectProjects = Project::all();
        $categories     = Category::all();

        return \view('modules.create',[

            'selected_navigation' => 'projects',
            'project'=> $project,
            'selectProjects' => $selectProjects,
            'categories' => $categories,
            'settings' => [],
            'project_id' => $project_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:150',
            'description' => 'required',
            'id'=>'nullable|integer',
            'start_date' => 'required',
            'end_date' => 'required',
            'project_id' => 'required',
            'category_id' => 'required',
            'skills'   =>  'required'
        ]);

        $module = false;

        if($request->id){
            $module = Module::find($request->id);
        }

        if(!$module ){
              $module  = new Module();
        }

        $module->name = $request->name;
        $module->project_id = $request->project_id;
        $module->category_id = $request->category_id;
        $module->start_date = $request->start_date;
        $module->end_date = $request->end_date;
        $module->summary = $request->summary;
        $module->description = ($request->description);

        $skills = $request->get("skills");

        if($module->save()){
            foreach ($skills as $skill){
                $moduleSkill = new ModuleSkill();
                $moduleSkill->skill_id = $skill;
                $moduleSkill->module_id = $module->id;
                $moduleSkill->save();
            }
        }

        if ($request->has('staff') && !empty($request->staff)){

//            try{
                $user = User::find($request->staff);
                $this->assignModuleUtility($user, $module);

                return redirect()->route('modules.index',[
                    'staff'=> $request->staff
                ])->with("success", "Successfully added a module!");
//            }catch (\Exception $exception){
//                throw New \Exception("Problem with the action!",500);
//            }

        }

        return redirect()->route('modules.index',[
            'project_id'=> $request->project_id
        ])->with("success","Successfully added a module!");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::find($id);
        $selectProjects = Project::all();

        return view('modules.edit',[
            'module' => $module,
            'selectProjects' => $selectProjects,
            'settings' => []
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        Module::find($id)->update($request->all());

        return redirect()->route('modules.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignModule(Request $request){
        $user_id = $request->user_id;
        $module_id = $request->module_id;
        $user = User::find($user_id);
        $module = Module::find($module_id);

        if (UserModule::find($module_id)){
            return redirect()->route('modules.index',['staff'=> $user_id])
                ->with("info","This module has already been assigned!");
        }

        $this->assignModuleUtility($user, $module);

        return redirect()->route('modules.index',['staff'=> $user_id]);
    }

    public function moduleDocumentation($id){
        $module = Module::find($id);
        $tasks = Task::with("userTask")->whereModuleId($id)->get();

        return view("modules.documentation",[
            "tasks" => $tasks,
            "module" => $module
        ]);
    }


    private function assignModuleUtility(User $user, Module $module){

        \DB::beginTransaction();
            UserModule::create([
                "user_id" => $user->id,
                "module_id" => $module->id
            ]);
            $user->notify((new AssignModuleNotification($user, $module))->locale('en_US'));
        \DB::commit();
    }
}
