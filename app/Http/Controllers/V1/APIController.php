<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\FileModel;
use App\Models\FolderModel;
use App\Models\Skill;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;

class APIController extends Controller
{

    public function getSkills(Request $request){
        $query = $request->query('q');

//        $skill = Skill::whereRaw('LOWER(`name`) LIKE ? ',[trim(strtolower($query)).'%']);
        $skill = Skill::whereName(trim(strtolower($query)))->first();

        if ($skill){
            return $this->getSkillsQuery($query);
        }

        $response = $this->getAllSkills($query);

        foreach ( $response->json() as $skill){
            $skillModel = new Skill();
            $skillModel->name = strtolower($skill);
            $skillModel->save();
        }

        return $this->getSkillsQuery($query);


    }

    /**
     * @param $query
     * @return mixed
     */
    public function getSkillsQuery($query)
    {
        return Skill::where("name", "LIKE", "%$query%")->selectRaw("id,name as text")
            ->take(10)->get(["id", "name"]);
    }


    public function getFolderFiles(Request $request, $id): Collection
    {
        $query = $request->query("q");

         $searchArray = FileModel::where("folder_id",$id)
             ->where("file_name","LIKE", "%".$query."%")
             ->get();
//
         $searchArray = $searchArray->transform(function ($item, $key){
             return [
                 "text" => $item->file_name,
                 "id"=> $item->id
             ];
         });

         return $searchArray;
    }

    /**
     * @param $query
     * @return \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
     */
    public function getAllSkills($query)
    {
        return Http::withHeaders([
            'apiKey' => getenv('PROMPT_SKILLS_API_KEY'),
        ])->get(getenv('PROMPT_SKILLS_API_URL'), [
            'q' => $query,
            'count' => 20
        ]);
    }
}
