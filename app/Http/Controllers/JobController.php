<?php

namespace App\Http\Controllers;

use App\Models\GigJob;
use App\Models\Module;
use App\Models\Task;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class JobController extends BaseController
{


    public function index(){

//        $modules = Module::doesntHave('user')->get();
        $modules = Module::wherePrivate(false)->get();

        return view("board.jobs",[
            "modules" => $modules
        ]);
    }

    public function show($id){
        $module = Module::find($id);
        $tasks = Task::with("userTask")->whereModuleId($id)->get();

        return view("board.show",[
            "tasks" => $tasks,
            "module" => $module
        ]);
    }

    public function create(){

    }

    public function update(Request $request, $id){

    }

    public function dashboard(){
        $modules = Module::all();

        return view("board.dashboard",[
            "modules" => $modules
        ]);
    }


    public function showInterest(){

    }

    public function takeGig($module_id){

        $gig = GigJob::whereUserId($this->user->id)->whereModuleId($module_id)->first();

        if ($gig){
            GigJob::find($gig->id)->update([
                "applied" => true,
                "interested" => true
            ]);
        }else{
            GigJob::create([
                "user_id" => $this->user->id,
                "module_id" => $module_id,
                "applied" => true,
                "interested" => true
            ]);
        }

        return redirect()->back()->with("success","Successfully applied!");
    }

}
