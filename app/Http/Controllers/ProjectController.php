<?php

namespace App\Http\Controllers;

use App\Models\FileModel;
use App\Models\FolderModel;
use App\Models\Module;
use App\Models\Project;
use App\Models\ProjectFolder;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class ProjectController extends Controller
{

    public function projectDetails($id){
        $settings = [];

        $fileIds = ProjectFolder::whereProjectId($id)->pluck("file_id");

        $project = Project::with(["developerGuideFolder" => function($guide){
            $guide->with(["files" => function($file){
                return $file->orderBy("created_at","desc");
            }]);
        },"assetGuideFolder"=>function($asset){
             $asset->with(["files" => function($file){
                 return $file->orderBy("created_at","desc");
             }]);
         }])->whereId($id)->first();

       $specificPages = FileModel::whereIn("id",$fileIds)->get();

       $repositories = Http::get(getenv('BITBUCKET_URL'))["values"];

       $moduleIds = Module::whereProjectId($project->id)->pluck("id");
       $tasks = Task::with("userTask")->whereIn("module_id", $moduleIds)->limit(5)->get();

        return \view('project_detail',[
            'settings'      => $settings,
            'project'       => $project,
            'repositories'  => $repositories,
            'specificPages' => $specificPages,
            "tasks"         => $tasks
        ]);
    }

    public function addProject(){

        $settings = [];
        $recent_projects= Project::orderBy('id', 'desc')->get();
        $repositories = Http::get(getenv("BITBUCKET_URL"))["values"];
        $htmlFiles = FolderModel::all();

        return \view('add-project',[
            'settings' => $settings,
            'projects' => $recent_projects,
            'repositories' => $repositories,
            'htmlFiles'     => $htmlFiles,
        ]);
    }


    public function storeProject(Request $request){

        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'repository_url'=>'required',
            'developer_guide_folder_id'=>'required',
            'asset_library_doc_folder_id'=>'required',
        ]);

        $user_guide_file_name = "";

        try{
            $image_name = "";
            if ($request->hasFile("wireframe_url")){
                $image_name = time().".".$request->wireframe_url->extension();
                $request->wireframe_url->move(public_path('images'), $image_name);
            }

            $user_guide_file = $request->file("user_guide");
            $user_guide_file_name = time().".".$user_guide_file->extension();
            $user_guide_file->move(public_path("pdf"), $user_guide_file_name);
        }catch (\Exception $exception){

        }


        $project = Project::create([
            "name" => $request->name,
            "description"                  => $request->description,
            "repository_url"               => $request->repository_url,
            "developer_guide_folder_id"    => $request->developer_guide_folder_id,
            "asset_library_doc_folder_id"  => $request->asset_library_doc_folder_id,
            "user_guide"                   => $user_guide_file_name,
            "dev_url"                      => $request->dev_url,
            "wireframe_url"                => $image_name,
            "api_endpoints_url"            => $request->api_endpoints_url,
            "adobe_protopie_link"          =>  $request->adobe_protopie_link,
            "private"                      => $request->private == "on"
        ]);


        if (is_array($request->get("pages"))) {

            foreach ($request->get("pages") as $page) {
                ProjectFolder::create([
                    "file_id" => $page,
                    "project_id" => $project->id
                ]);
            }
        }

        return redirect('/dashboard');
    }

    /**
     * @param $doc_base_file
     * @return string
     */
    public function unzipFolder($doc_base_file): string
    {
        $extracted = false;
        $zip = new \ZipArchive;
        if ($zip->open($doc_base_file) === TRUE) {
            $zip->extractTo(public_path('html-assets'));
            $zip->close();
            $extracted = true;
        }

        return $extracted;
    }

}
