<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use App\Models\User;
use App\Models\UserSkill;
use App\Notifications\SNewUserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class ProfileController extends BaseController
{


    public function profile(){
        if (Auth::guest()){
            return redirect('/dashboard');
        }

        $settings = [];
        $user = Auth::user();
        return view('profile.profile', [
            'settings' => $settings,
            'user' => $user
        ]);
    }

    public function profilePost(Request $request){

        $request->validate([

            'first_name'=>'nullable|string|max:100',
            'last_name'=>'nullable|string|max:100',
            'photo' => 'nullable|file|mimes:jpg,png',

        ]);

//        $user  = $this->user;
        $user  = Auth::user();


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->language = $request->language;
        $path = false;
        if($request->photo)
        {
            $path = $request->file('photo')->store('public');
            $path = str_replace('public/','',$path);


        }
        $user->photo = $path;

        $user->phone_number = $request->phone_number;
        $user->save();


        return redirect('/profile');
    }

    public function newUser(Request $request)
    {
        $request->validate([
            'id' => 'nullable|integer',
        ]);
//        $countries = countries();

        $selected_user = false;

        if($request->id)
        {
            $selected_user = User::find($request->id);
        }


        return \view('profile.new-user',[
            'selected_navigation' => 'staff',
            'selected_user'=> $selected_user,
            'countries'=> [],
            'settings' => []

        ]);



    }

    public function staff()
    {

        $staffs = User::with(['skills'=> function($skill){
            return $skill->with('skill:id,name')->get();
        }])->get();

//        return $staffs[0]->skills[0];

        return \view('profile.staff',[
            'settings' => [],
            'selected_navigation' => 'team',
            'staffs' => $staffs
        ]);

    }

    public function userPost(Request $request)
    {

        $request->validate([

            'first_name'=>'required|nullable|string|max:100',
            'last_name'=>'required|nullable|string|max:100',
            'email'=>'required|email',
            'phone'=>'unique:users|nullable|string|max:50',
            'password'=>'required|string|max:50',
            'id'=>'nullable|integer',
            'development_role' => 'required',
            'skill_level' => 'required'
        ]);


        $user = false;

        if($request->id)
        {
            $user = User::find($request->id);
        }


        if(!$user)
        {
            $user = new User();
        }

//        DB::beginTransaction();

            $cv_file_name = "";
            if ($request->hasFile("cv")){
                $cv_file_name = time().".".$request->cv->extension();
                $request->cv->move(public_path(),$cv_file_name);
            }

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->password = Hash::make($request->password);
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->mobile_number = $request->mobile_number;
            $user->development_role = $request->development_role;
            $user->skill_level = $request->skill_level;
            $user->cv = $cv_file_name;
//            $user->twitter = $request->twitter;
//            $user->facebook = $request->facebook;
            $user->linkedin = $request->linkedin;
            $user->address_1 = $request->address_1;
            $user->address_2 = $request->address_2;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->country = $request->country;
            $user->language = $request->language;
            $user->zip = $request->zip;
            if($user->save()){
//                $skills = $request->get("skills");
//
//                foreach ($skills as $skill){
//                    $skillObject = new UserSkill();
//                    $skillObject->user_id = Auth::check() ? Auth::user()->id : null;
//                    $skillObject->skill_id = $skill;
//                    $skillObject->save();
//                }
            }

//            DB::commit();

        $user->notify((new SNewUserNotification($user))->locale('en_US'));

        return redirect('/staff');
    }

    public function userChangePasswordPost(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'new_password' => 'required|confirmed'
        ]);

        $user = Auth::user();

        if(!Hash::check($request->password,$user->password))
        {
            return redirect('/profile')->withErrors([
                'password' => 'Incorrect old password.'
            ]);
        }

        if(config('app.environment') !== 'demo')
        {
            $user->password = Hash::make($request->new_password);
            $user->save();
        }

        return redirect('/profile');
    }

}
