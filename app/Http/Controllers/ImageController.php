<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{

    public  function index()
    {
        return view('welcome');
    }

    public  function dropZone(Request $request)
    {
        $file = $request->file('file');
        $fileName = time().'.'.$image->extension();
        $file->move(public_path('images'),$fileName);
        return response()->json(['success'=>$fileName]);
    }
}
