<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Task;
use App\Models\UserTask;
use App\Notifications\TaskStatusNotification;
use Illuminate\Http\Request;

class TaskController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $create_url = '/tasks/create';
        if (\request()->has('module_id')){
            $tasks = Task::whereModuleId(\request()->query('module_id'))->get();
            $create_url = route('tasks.create',["module_id" => \request()->query("module_id")]);
        }else{
            $tasks = Task::all();
        }

        return view('tasks.index',[
            'tasks' => $tasks,
            "create_url" => $create_url
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $module_id = \request()->query('module_id');
        $modules = Module::all();

        return view('tasks.create',[
            'modules' => $modules,
            'module_id' => $module_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
//        return $request->all();
        $request->validate([
            'name'=>'required|max:150',
            'description' => 'required',
            'points' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        Task::create($request->except("exit"));

        if ($request->has("exit"))
            return redirect()->route('tasks.index')->with("success","Successfully added a task!");;
        return redirect()->route('tasks.create')->with("success","Successfully added a task!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = $request->status;
        $taskObject  = Task::find($id);
        $update = $taskObject->update(["status" => $status]);

        if ($status == "Finished"){
            $user = \Auth::user();
            $user->notify((new TaskStatusNotification($user, $taskObject))->locale('en_US'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function document($id){

        $documentation = UserTask::whereTaskId($id)->whereUserId($this->user->id)->selectRaw("documentation")->first();

        return view("tasks.document",[
            "task_id" => $id,
            "documentation" => $documentation ? $documentation->documentation : ""
            ]);
    }

    public function documentTask(Request $request){
        $request->validate([
//            "documentation" => "required"
        ]);

        $userTask = UserTask::whereTaskId($request->id)->whereUserId($this->user->id)->first();

        if ($userTask){
            $userTask->update([
                "documentation" => $request->documentation
            ]);
        }else {

            UserTask::create([
                "user_id" => $this->user->id,
                "task_id" => $request->id,
                "documentation" => $request->documentation
            ]);
        }

        return redirect()->back()->with("success", "Successfully documented!");
    }
}
