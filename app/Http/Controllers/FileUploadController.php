<?php

namespace App\Http\Controllers;

use App\Models\FileModel;
use App\Models\FolderModel;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileUploadController extends BaseController
{

    public function fileUploads(){

        $s3_folders = Storage::disk('s3')->directories();
        return view('uploads.index', [
            's3folders' => $s3_folders
        ]);
    }

    /**
     * File Upload Method
     *
     * @return
     */

    public  function dropzoneFileUpload(Request $request)
    {

        $request->validate([
            'type'=>'required',
            'file'=>'required',
        ]);

        $file = $request->file('file');
        $mimeType = $request->get('type');
        $file_name = str_replace(" ","-",substr($file->getClientOriginalName(),0,-5)).".".$file->extension();

         Storage::disk('s3')->put("$file_name",file_get_contents($request->file('file')->getRealPath()), [
            'visibility' => 'public'
        ]);

        return redirect()->back();
    }

    public function uploadZips(Request $request){

        $request->validate([
            'file'=>'required|mimes:zip',
        ]);

        $file = $request->file("file");

       $folder = FolderModel::whereOriginalName($file->getClientOriginalName())->first();
       if ($folder)
        return redirect()->back()->with('error','Folder already exist!');

        $this->unzipFolder($file);
        return redirect()->back()->with("success", "Successfully uploaded files!");

    }

    /**
     * @param UploadedFile $base_file
     * @return void
     */
    public function unzipFolder(UploadedFile $base_file): void
    {
        $file_name = $base_file->getClientOriginalName();
        $stripped_file_name = substr($file_name,0,-4);
        $extracted = false;
        $zip = new \ZipArchive;
        if ($zip->open($base_file) === TRUE) {
            $zip->extractTo(public_path("html-assets/$stripped_file_name"));
            $zip->close();
            $extracted = true;
        }

        if ($extracted){
            $this->saveFolderFiles($file_name,$stripped_file_name);
        }

//        return $extracted;
    }

    /**
     * @param $stripped_file_name
     */
    public function saveFolderFiles($file_name,$stripped_file_name): void
    {
        $htmlFiles = scandir(public_path("html-assets/$stripped_file_name"));

        $folder = FolderModel::create([
            "name" => $stripped_file_name,
            "original_name" => $file_name
        ]);

        foreach ($htmlFiles as $key => $file) {
            if ($folder) {
                FileModel::create([
                    "file_name" => $file,
                    "file_url" => "$stripped_file_name/$file",
                    "folder_id" => $folder->id
                ]);
            }
        }

//        $this->indexModel();
    }

    private function indexModel(){
        \Artisan::call("scout:import",["model" => "App\\Models\\FileModel" ]);
    }
}
