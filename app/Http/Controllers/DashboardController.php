<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Project;
use App\Models\User;
use App\Models\UserModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class DashboardController extends BaseController
{
//    use \ZipArchive;

    public function dashboard()
    {
        $settings = [];

        $builder = Project::orderBy('id', 'desc');

            if (Auth::user()->admin){
                $recent_projects = $builder->get();
            }else{
                $moduleIds = UserModule::whereUserId(Auth::user()->id)->pluck("module_id");
                $projectIds = Module::whereIn("id",$moduleIds)->pluck("project_id");
                $recent_projects = $builder->whereIn("id",$projectIds)->get();
            }

        return \view('index',[
            'settings' => $settings,
            'projects' => $recent_projects
        ]);
    }

    public function projectDetails($id){
        $settings = [];

        $project = Project::find($id);
        $urls = explode(",",$project->asset_url);
        $repositories = Http::get(getenv('BITBUCKET_URL'))["values"];




        return \view('project_detail',[
            'settings' => $settings,
            'project' => $project,
            'urls'   => $urls,
            'repositories' => $repositories
        ]);
    }

//    public function addProject(){
//
//        $settings = [];
//        $recent_projects= Project::orderBy('id', 'desc')->get();
//        $repositories = Http::get(getenv("BITBUCKET_URL"))["values"];
//        $s3_folders = Storage::disk('s3')->directories();
//
//        return \view('add-project',[
//            'settings' => $settings,
//            'projects' => $recent_projects,
//            'repositories' => $repositories,
//            's3folders'     => $s3_folders
//        ]);
//    }

//    public function storeProject(Request $request){
////        $validated =  $request->validate([
////            'name'=>'required',
////            'description'=>'required',
////            'repository_url'=>'required',
////            'asset_url'=>'required',
////            'doc_base_url'=>'required',
////        ]);
//
////        $s3_base_url = getenv('S3_WEBSITE_BASE_URL');
//
////        $strip_spaces_from_asset_url = str_replace(" ","-",$request->asset_url);
////        $folder_path = str_replace(".zip","",$strip_spaces_from_asset_url);
//
////        $strip_spaces_from_doc_base_url = str_replace(" ","-",$request->doc_base_url);
////        $get_file_extension = substr($strip_spaces_from_doc_base_url,-4);
////        $doc_folder_path = str_replace($get_file_extension,"",$strip_spaces_from_doc_base_url);
//
//        $doc_base_file = $request->file("doc_base_url");
//        $asset_file = $request->file("doc_base_url");
//////        $doc_base_file_name = str_replace(" ","-",substr($doc_base_file->getClientOriginalName(),0,-5)).".".$doc_base_file->extension();
////        $doc_base_file_name = str_replace(" ","-",substr($doc_base_file->getClientOriginalName(),0,-4));
////        $asset_file_name = str_replace(" ","-",substr($doc_base_file->getClientOriginalName(),0,-4));
//
//
//        $zip = new \ZipArchive;
//        if ($zip->open($doc_base_file) === TRUE) {
//            $zip->extractTo(public_path(''));
//            $zip->close();
////            echo 'ok';
//            return  'ok';
//        } else {
//            return  'failed';
//        }
//
//
////        $zip = ZipArchive($doc_base_file);
////        $this->unZipFile($zip);
////
////        $asset_zip = zip_open($asset_file);
////        $this->unZipFile($asset_zip);
//
//        $image_name = time().".".$request->wireframe_url->extension();
//        $request->wireframe_url->move(public_path('images'), $image_name);
//
////        $project = Project::create([
////            "name" => $request->name,
////            "description"       => $request->description,
////            "repository_url"    => $request->repository_url,
////            "asset_url"         => $s3_base_url.$folder_path,
////            "doc_base_url"      => $s3_base_url.$doc_folder_path,
////            "dev_url"           => $request->dev_url,
////            "wireframe_url"     => $image_name,
////            "api_endpoints_url" => $request->api_endpoints_url,
////        ]);
//
//        $project = Project::create([
//            "name" => $request->name,
//            "description"       => $request->description,
//            "repository_url"    => $request->repository_url,
//            "asset_url"         => $asset_file_name,
////           "doc_base_url"     => $request->base_url,
//            "doc_base_url"      => $doc_base_file_name,
//            "dev_url"           => $request->dev_url,
//            "wireframe_url"     => $image_name,
//            "api_endpoints_url" => $request->api_endpoints_url,
//        ]);
//
//
//
////
//
//        return redirect('/dashboard');
//    }

    public function editProject($id){
        $settings = [];
        $project = Project::find($id);
        $projects = Project::all();

        return view('edit-project', [
            'project' => $project,
            'projects' => $projects,
            'settings' => $settings
        ]);
    }


    public function updateProject($id,Request $request){

        $project_builder = Project::find($id);
        $project = $project_builder->update($request->except(['id','private']));

        if ($project){
            $project_builder->update(["private" => $request->private == "on"]);
        }

        return redirect('/dashboard')->with("success","Successfully updated!");
    }

    /**
     * @param $zip
     */
    public function unZipFile($zip): void
    {
        if ($zip) {
            while ($zip_entry = zip_read($zip)) {
                $fp = fopen(zip_entry_name($zip_entry), "w");
                if (zip_entry_open($zip, $zip_entry, "r")) {
                    $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                    fwrite($fp, "$buf");
                    zip_entry_close($zip_entry);
                    fclose($fp);
                }
            }
            zip_close($zip);
        }
    }


}
