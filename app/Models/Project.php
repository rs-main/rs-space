<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Project
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $repository_url
 * @property string|null $asset_url
 * @property string|null $doc_base_url
 * @property string|null $dev_url
 * @property string|null $wireframe_url
 * @property string|null $api_endpoints_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project query()
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereApiEndpointsUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereAssetUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDevUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDocBaseUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereRepositoryUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereWireframeUrl($value)
 * @mixin \Eloquent
 */
class Project extends Model
{
    use HasFactory;

    protected $guarded =  [];

    public function developerGuideFolder(){
        return $this->belongsTo(FolderModel::class,"developer_guide_folder_id");
    }

    public function assetGuideFolder(){
        return $this->belongsTo(FolderModel::class,"asset_library_doc_folder_id");
    }


}
