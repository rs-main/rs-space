<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * App\Models\UserSkill
 *
 * @property int $id
 * @property int $user_id
 * @property int $skill_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Skill $skill
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill whereSkillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSkill whereUserId($value)
 * @mixin \Eloquent
 */
class UserSkill extends Model
{
    use HasFactory;

    protected $table = "user_skill";
    protected $guarded = [];

    public function skill(){
        return $this->belongsTo(Skill::class, 'skill_id');
    }
}
