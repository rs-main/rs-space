<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserModule
 *
 * @property int $id
 * @property int $user_id
 * @property int $module_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserModule whereUserId($value)
 * @mixin \Eloquent
 */
class UserModule extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = "user_modules";

    public function user(){
        return $this->hasOne(User::class,'user_id',"id");
    }


    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
