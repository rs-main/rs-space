<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GigJob extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function applied($module_id){
        $user_id = \Auth::user()->id;
        $gig = GigJob::whereUserId($user_id)->whereModuleId($module_id)->first();
        return (bool)$gig;
    }
}
