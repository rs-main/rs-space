<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ModuleSkill
 *
 * @property int $id
 * @property int $module_id
 * @property int $skill_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Skill $skill
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill whereSkillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModuleSkill whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ModuleSkill extends Model
{
    use HasFactory;

    protected $table = "module_skill";
    protected $guarded = [];

    public function skill(){
        return $this->belongsTo(Skill::class,"skill_id");
    }

}
