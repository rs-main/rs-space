<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Task
 *
 * @property int $id
 * @property string $name
 * @property int $module_id
 * @property string|null $start_date
 * @property string|null $end_date
 * @property string $status
 * @property string|null $description
 * @property string|null $summary
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $points
 * @method static \Illuminate\Database\Eloquent\Builder|Task wherePoints($value)
 */
class Task extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function module(){
        return $this->belongsTo(Module::class, 'module_id');
    }

    public function userTask(){
        return $this->hasOne(UserTask::class, "task_id");
    }

    public static function PointsPercentage($module_id){
        $completed_task_points = Task::whereModuleId($module_id)->whereStatus("Finished")->sum("points");
        $moduleTotalPoints = Task::whereModuleId($module_id)->sum("points");
        if ($moduleTotalPoints == 0) return 0;
        return ($completed_task_points/$moduleTotalPoints)* 100;
    }
}
