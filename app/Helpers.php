<?php

namespace App;

use Illuminate\Support\Facades\URL;

class  Helpers
{

    public static function getTemporaryUrl($route, $query_parameters=[], $minutes=30){
        return URL::temporarySignedRoute(
            $route, now()->addMinutes($minutes), $query_parameters
        );
    }

}
