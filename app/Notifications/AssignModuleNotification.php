<?php

namespace App\Notifications;

use App\Models\Module;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class AssignModuleNotification extends Notification
{
    use Queueable;

    private $user;
    private $module;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Module $module)
    {
        $this->user = $user;
        $this->module = $module;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
//        return ['mail'];
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $url = '#';
        $cv_url = url("/viewer?url=".$this->user->cv);

        return (new SlackMessage)
            ->success()
            ->content('Module Assigned to a Developer!')
            ->attachment(function ($attachment) use ($url, $cv_url) {
                $attachment->title($this->user->first_name." " .$this->user->last_name, $url)
                    ->fields([
                        'Module' => $this->module->name,
                        'Developer' => $this->user->first_name." ".$this->user->last_name,
                        'Start' => $this->module->start_date,
                        'End' => $this->module->end_date
                    ]);
            });
    }
}
