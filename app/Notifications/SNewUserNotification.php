<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class SNewUserNotification extends Notification
{
    use Queueable;

    private $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
//        return ['mail'];
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Welcome to Real Studios Ltd.')
                    ->action('Notification Action', url('/'))
                    ->line('You are signed up on our platform!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $url = '#';
        $cv_url = url("/viewer?url=".$this->user->cv);

        return (new SlackMessage)
            ->success()
            ->content('New Developer is on board!')
            ->attachment(function ($attachment) use ($url, $cv_url) {
                $attachment->title($this->user->first_name." " .$this->user->last_name, $url)
                    ->fields([
                        'Full Name' => $this->user->first_name." ".$this->user->last_name,
                        'Role' => $this->user->development_role,
                        'Phone' => $this->user->phone_number,
                        'CV' => $cv_url,
                    ]);
            });
    }
}
