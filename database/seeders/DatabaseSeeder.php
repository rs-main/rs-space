<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\FileModel;
use App\Models\FolderModel;
use App\Models\Project;
use App\Models\ProjectFolder;
use App\Models\Quote;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call("db:wipe");
        Artisan::call("migrate");
        $folderPath = public_path('html-assets/');

        foreach (FolderModel::all() as $folder) {
             \File::deleteDirectory("$folderPath/$folder");
        }

        $user = User::first();
        if(!$user)
        {
            $user = new User();
            $user->first_name = 'Admin';
            $user->last_name = 'Muni';
            $user->email = 'admin@realstudiosonline.com';
            $user->password = Hash::make('123456');
            $user->status   = "engaged";
            $user->admin    = true;
            $user->cv       = "cv.pdf";
            $user->development_role = "Android";
            $user->save();
        }

        $categories = ["Android","Web Frontend","Web Backend", "IOS"];

        foreach ($categories as $category){
            Category::create([
                "name" => $category
            ]);
        }

        $text = '  <div class="text-center mt-8">
                        <h6>Executive Summary</h6>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>
                    </div>
                    <div class="text-center mt-4">
                        <h6>Company description</h6>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>
                    <div class="text-center mt-4">
                        <h6>Market Analysis</h6>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>

                    <div class="text-center mt-4">
                        <h6>Organization &amp; Management</h6>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>
                    <div class="text-center mt-4">
                        <h6>Service or product</h6>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>
                    <div class="text-center mt-4">
                        <h6>Investment/Funding request</h6>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>
                    <div class="text-center mt-4">
                        <h6>Financial projections</h6>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui fringilla consectetur id nec massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis. Duis nunc eros, mattis at dui ac, convallis sem</p>
                </div>';



//        for ($i = 0; $i < 100; $i++) {
//            Project::create([
//                "name" => "PROJECT-$i",
//                "description" => $text,
//                "repository_url" => "https://bitbucket.org/real-studios-dev/dextraclass.git",
//                "developer_guide_folder_id" => 1,
//                "asset_library_doc_folder_id" => 1,
//                "user_guide" => "https://xtraclass.assets.realstudiosonline.com/docs/XtraClass_web_ux.pdf",
//                "dev_url" => "https://dextraclass.com",
//                "wireframe_url" => "1630084555.png",
//                "api_endpoints_url" => "https://dextraclass.com/api/documentation"
//            ]);
//
//            foreach ($request->get("pages") as $page) {
//                ProjectFolder::create([
//                    "file_id" => $page,
//                    "project_id" => $project->id
//                ]);
//            }
//        }

        $quotes = Http::get('https://type.fit/api/quotes')->json();
         foreach ($quotes as $quote){
             Quote::create([
                 "text" => $quote["text"],
                 "author" => $quote["author"]
             ]);
         }
    }


    /**
     * @param $stripped_file_name
     */
    public function saveFolderFiles(): void
    {
        $htmlFiles = scandir(public_path("html-assets/extra-pages"));

        $folder = FolderModel::create([
            "name" => "extra-pages",
            "original_name" => "extra-pages.zip"
        ]);

        foreach ($htmlFiles as $key => $file) {
            if ($folder) {
                FileModel::create([
                    "file_name" => $file,
                    "file_url" => "extra-pages/$file",
                    "folder_id" => $folder->id
                ]);
            }
        }

//        $this->indexModel();
    }

    private function indexModel(){
        \Artisan::call("scout:import",["model" => "App\\Models\\FileModel" ]);
    }

}
