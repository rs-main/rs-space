<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->unsignedBigInteger("module_id");
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->enum('status',[
                'Pending',
                'Started',
                'Finished',
            ])->default('Pending');
            $table->text('description')->nullable();
            $table->text('summary')->nullable();
            $table->integer("points")->default(0);
            $table->boolean("approved")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
