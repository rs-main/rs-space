<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->unsignedBigInteger("project_id");
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->enum('status',[
                'Pending',
                'Started',
                'Finished',
            ])->default('Pending');
            $table->text('description')->nullable();
            $table->text('summary')->nullable();
            $table->unsignedBigInteger("category_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
