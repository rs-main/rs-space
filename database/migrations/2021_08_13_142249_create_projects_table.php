<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("description")->nullable();
            $table->string("repository_url")->nullable();
            $table->unsignedBigInteger("developer_guide_folder_id")->nullable();
            $table->unsignedBigInteger("asset_library_doc_folder_id")->nullable();
            $table->string("user_guide")->nullable();
            $table->string("dev_url")->nullable();
            $table->text("wireframe_url")->nullable();
            $table->text("adobe_protopie_link")->nullable();
            $table->string("api_endpoints_url")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
