<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGigJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gig_jobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("module_id");
            $table->unsignedBigInteger("user_id");
            $table->boolean("interested");
            $table->boolean("applied");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gig_jobs');
    }
}
